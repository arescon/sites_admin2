import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { dateFormater } from '../../utils';

export default class Table extends Component {
  constructor(props){
    super(props);
  }
  componentDidMount() {
    $('.dropdown-toggle').dropdown();
  }
  render() {
    let config = this.props._config || [],
        data = this.props._data || [],
        actionFunc = this.props.actionFunc,
        clickFunc = this.props.clickFunc,
        actions = this.props.actions || [];
    return (
      <table className="table">
        <thead>
          <tr>
            {
              config.map((k,_i)=>{
                return <th key={`th_${_i}`}>{k.title}</th>
              })
            }
            {
              (actions && actions.length > 0) ? <th>Действия</th> : null
            }
          </tr>
        </thead>
        <tbody>
          {
            data.map((el,_i)=>{
              return (
                <tr key={`tr_${_i}`} style={{ cursor: 'pointer'}}>
                  {
                    config.map((k,_ia)=>{
                      switch (k.type) {
                        case 'array':
                          return <td key={`tr_${_ia}`} onClick={() => clickFunc ? clickFunc(el) : null}>
                            <ul className="list-unstyled">
                              {
                                (Array.isArray(el[k.item])) ? el[k.item].map((el,i)=> {
                                    return <li key={`li_${_ia}_${i}`}>{el}</li>;
                                  }) : null
                              }
                            </ul>
                          </td>
                          break;
                        case 'text':
                          return <td key={`tr_${_ia}`} onClick={() => clickFunc ? clickFunc(el) : null}>{el[k.item]}</td>
                          break;
                        case 'img':
                          return <td key={`tr_${_ia}`} onClick={() => clickFunc ? clickFunc(el) : null}>
                              <img src={el[k.item][0].filepath || ''} height='100'/>
                            </td>
                          break;
                        case 'datetime':
                          return <td key={`tr_${_ia}`} onClick={() => clickFunc ? clickFunc(el) : null}>{dateFormater(el[k.item], true)}</td>
                          break;
                        default:
                          return <td key={`tr_${_ia}`} onClick={() => clickFunc ? clickFunc(el) : null}> {JSON.stringify(el[k.item])}</td>
                      }
                      return k;
                    })
                  }
                  {
                    (actions && actions.length > 0) ?
                      (
                        <td>
                          <div className="dropdown">
                            <button className="btn btn-default btn-xs dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                              <span className="glyphicon glyphicon-cog" aria-hidden="true"></span>
                              <span className="caret"></span>
                            </button>
                            <ul className="dropdown-menu" aria-labelledby="dropdownMenu1">
                              {
                                actions.map((act, k) => {
                                  if (act.method === 'GET') {
                                    let _url, _id;
                                    if(this.props.id) {
                                      _id = this.props.id;
                                      _url = `/${act.url}/${_id}/${el.id}`
                                    } else _url = `/${act.url}/${el.id}`
                                    return (
                                      <li key={`_k${k}`}>
                                        <Link 
                                          key={`_k${k}`}
                                          to={_url}
                                          title={act.name}
                                        >
                                          <i className={'glyphicon ' + act.icon}/>
                                          {act.name}
                                        </Link>
                                      </li>
                                    )
                                  } else {
                                    return (
                                      <li key={`_k${k}`}>
                                        <a
                                          className='circular ui link icon item'
                                          style={{
                                            cursor: 'pointer'
                                          }}
                                          title={act.name}
                                          data-id={el.id}
                                          data-el={JSON.stringify(act)}
                                          data-func_name={act.url ? act.url : ''}
                                          onClick={actionFunc}
                                        >
                                          <i className={'glyphicon ' + act.icon}/>
                                          {act.name}
                                        </a>
                                      </li>
                                    )
                                  }
                                })
                              }
                            </ul>
                          </div>
                        </td>
                      )
                      : null
                  }
                </tr>
              )
            })
          }
        </tbody>
      </table>
    )
  }
}
/*
actions: [
  {
    icon:"edit",
    id:0,
    method:"GET",
    name:"Редактировать",
    url:"mtype"
  }
]
*/
// <li role="separator" className="divider"></li>
/*
case 'array':
    if(Array.isArray(el)){
      if(el[k].length > 0) {
        <td key={`tr_${_ia}`}>
          <ol className="rounded-list">
            {
              el[k].map((_d)=>{
                return (
                  <li>
                    { _d }
                  </li>
                )                               
              })
            }
          </ol>
        </td>
      }
    }
  break;
*/