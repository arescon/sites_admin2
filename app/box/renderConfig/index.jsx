/*
  название: Модуль рендеринга по конфигу
  описание:
    layout - конфиг
    data - данные
    handlerChange - callback
*/
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import md5 from 'md5';
import Table from '../table/index.jsx';
import Modal from '../modal/index.jsx';
import SelectBox from '../select/index.jsx';
import FilePicker from '../file_picker/index.jsx';
import CKEditor from "react-ckeditor-component";
import { escapeHtml } from '../../utils';
import { isNumber } from 'util';
// escapeHtml

export default class RenderConfig extends Component {
  constructor(props) {
    super(props);
    this.state = ({
      new: {}
    })
  }
  handlerClockLink(val) {
    $('.modal-backdrop').addClass('hidden');
  }
  render() {
    // console.log('renderConfig',this.props);
    let self = this;
    return this.props.layout.map((val, i, arr)=>{
      let disabled = val.disable ? {'disabled' : 'disabled'} : {};
      switch (val.tag){
        case 'label':
        case 'div':
        case 'span':
        case 'p':
          return (
            <val.tag key={`it_${i}`} className={val.class || ''}>
              {
                val.title || ''
              }
              {
                val.childs ? (
                  <RenderConfig
                    _state={self.props._state || {}}
                    layout={val.childs || []}
                    url={this.props.url}
                    id={this.props.id}
                    parent={this.props.parent}
                    modal={this.props.modal}
                    handlerChange={(item, value)=>self.props.handlerChange(item, value)}
                    handlerSuccess={(type)=>self.props.handlerSuccess(type)}
                    handlerCancel={(type)=>self.props.handlerCancel(type)}
                    handlerDelete={(type)=>self.props.handlerDelete(type)}
                    handlerActions={(el)=>self.props.handlerActions(el)}
                  />
                ) : null 
              }
            </val.tag>
          )
          break;
        case 'img':
          <val.tag
            key={`it_${i}`}
            className={val.class || ''}
            src={val.value || this.props._state.data[val.item] || '' }
          />
          break;
        case 'input':
          return (
            <val.tag
              key={`it_${i}`}
              className={val.class || ''}
              type={val.type || 'text'}
              value={ this.props._state.data[val.item] || '' }
              placeholder={val.placeholder || ''}
              onChange={(ev)=>{
                this.props.handlerChange(val.item || null, ev.target.value)
              }}
              { ...disabled }
            />
          )
          break;
        case 'button':
          switch (val.type) {
            case 'links':
              /*
                {
                  tag: 'button',
                  type: 'links',
                  url: '/menu_items',
                  item: 'id',
                  parent: 'parent',
                  source: 'data',
                  class: 'btn btn-info',
                  label: 'Добавить пункт меню'
                }
              */
              let id_;
              if(val.source) {
                id_ = this.props._state.data[val.item] || 0;
              } else {
                id_ = this.props._state.data[val.item] || 0;
              }
              let _url = ''
              if(val.parent) {
                _url = `${val.url}/${this.props[val.parent]}/${id_}`
              } else _url = `${val.url}/${id_}`
              if(val.behaviors) {
                if(val.behaviors.visible) {
                  switch (val.behaviors.visible) {
                    case 1:
                      return (
                        <Link
                          key={`it_${i}`}
                          to={_url}
                          onClick={()=>this.handlerClockLink(val)}
                          className={val.class}
                        >
                          {val.label}
                        </Link>
                      )
                      break;
                    case 2:
                      if(this.props._state.data[val.item]) {
                        return (
                          <Link
                            key={`it_${i}`}
                            to={_url}
                            onClick={()=>this.handlerClockLink(val)}
                            className={val.class}
                          >
                            {val.label}
                          </Link>
                        )
                      } else return null;
                      break;
                    case 0:
                      return null;
                      break;
                  }
                }
              } else return (
                <Link
                  key={`it_${i}`}
                  to={_url}
                  onClick={()=>this.handlerClockLink(val)}
                  className={val.class}
                >
                  {val.label}
                </Link>
              )
              break;
            case 'link':
              /*
                {
                  tag: 'button',
                  type: 'link',
                  url: '/menu_items',
                  item: 'id',
                  class: 'btn btn-info',
                  label: 'Добавить пункт меню'
                }
              */
              return (
                <Link
                  key={`it_${i}`}
                  to={`${val.url}/${this.props._state.data[val.item] || 0}`}
                  className={val.class}
                >
                  {val.label}
                </Link>
              )
              break;
          }
          return (
            <val.tag
              key={`it_${i}`}
              className={val.class || ''}
              type={val.type || 'text'}
              value={ this.props._state.data[val.item] || '' }
              placeholder={val.placeholder || ''}
              onChange={(ev)=>{
                this.props.handlerChange(val.item || '', ev.target.value)
              }}
              { ...disabled }
            />
          )
          break;
        case 'table':
          let arr = [];
          if(val.item) {
            arr = this.props._state.data[val.item] || []
          } else {
            if (this.props._state.data) {
              if(Array.isArray(this.props._state.data)){
                arr = this.props._state.data
              } else arr = []
            } else arr = []
          }
          return (
            <Table
              key={`it_${i}`}
              _config={val.items || []}
              actions={val.actions || []}
              id={this.props.id || null}
              actionFunc={(el)=>this.props.handlerActions(el)}
              _data={arr}/>
            )
          break;
        case 'form_a':
          return <div key={`it_${i}`}>{ val.item }</div>
          break;
        case 'editor':
          //var defaultScriptUrl = 'https://cdn.ckeditor.com/4.10.1/full/ckeditor.js';
          return  <CKEditor 
                  activeClass="p10" 
                  content={ this.props._state.data[val.item] || '' } 
                  config={{
                    removeButtons: 'Cut,Copy,Paste'
                  }}
                  events={{
                    "change": (ev) => {
                      this.props.handlerChange(val.item || null, ev.editor.getData());
                    }
                  }}
                />
          /*
            return <Editor
              key={`it_${i}`}
              value={ this.props._state.data[val.item] || '' }
              init={{
                plugins: `print preview fullpage searchreplace autolink directionality
                visualblocks visualchars fullscreen image link media template codesample
                table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist
                lists textcolor wordcount imagetools contextmenu colorpicker textpattern help`,
                toolbar: 'formatselect | fontselect fontsizeselect bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
                language: 'ru',
                fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt"
              }}
              onChange={(ev)=>{
                this.props.handlerChange(val.item || null, ev.target.getContent());
              }}
            />
          */
          break;
        case 'file_picker':
          return <FilePicker
            key={`it_${i}`}
            _ID={val.item}
            config={{
              api: 'files',
            }}
            _params={val.params || []}
            files={ this.props._state.data[val.item] || [] }
            onChange={(array)=>{
              this.props.handlerChange(val.item || null, array)
            }}
          />
          break;
        case 'select':
          let _d = val.item;
          let _val = this.props._state.data[_d];
          return (
            <SelectBox
              key={`it_${i}`}
              url={this.props.url}
              id={this.props.id}
              parent={this.props.parent}
              className={val.class || ''}
              value={ !isNumber(_val) ? parseInt(_val) : _val || '' }
              placeholder={ val.placeholder || '' }
              config={val.config || {}}
              onChange={(selected)=>{
                this.props.handlerChange(val.item || null, selected)
              }}
              { ...disabled }
            />
          )
          break;
        case 'modal':
          let md_id = md5(Math.random()*(999-1)+1)
          return (
            <Modal
              key={`it_${i}`}
              classN={val.classN}
              class={val.class}
              md_id= {md_id}
              title={val.title || null}
              handlerSuccess={(type)=>this.props.handlerSuccess(type)}
              handlerCancel={(type)=>this.props.handlerCancel(type)}
              handlerDelete={(type)=>this.props.handlerDelete(type)}
            >
              {
                val.childs ? (
                  <RenderConfig
                    _state={this.props._state || {}}
                    layout={val.childs || []}
                    url={this.props.url}
                    id={this.props.id}
                    modal={md_id}
                    parent={this.props.parent}
                    handlerChange={(item, value)=>this.props.handlerChange(item, value)}
                    handlerSuccess={(type)=>this.props.handlerSuccess(type)}
                    handlerCancel={(type)=>this.props.handlerCancel(type)}
                    handlerDelete={(type)=>this.props.handlerDelete(type)}
                    handlerActions={(el)=>this.props.handlerActions(el)}
                  />
                ) : null 
              }
            </Modal>
          )
          break;
      }
    })
  }
}
 /*
         case 'select':
        console.log('sel', val);
          return (
            <Select
              key={`it_${i}`}
              className={val.class || ''}
              type={val.type || 'text'}
              value={ this.props._state.data[val.item] || '' }
              placeholder={val.placeholder || ''}
              config={val.config || {}}
              onChange={(selected)=>{
                this.props.handlerChange(val.item || null, selected)
              }}
              { ...disabled }
            />
          )
          break;
 */