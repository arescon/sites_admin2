import React from 'react';
import update from 'immutability-helper';
import { Axios, LastIndex as LastItem, getBase64 } from '../../utils';
import { randomFillSync } from 'crypto';
let _api = new Axios();
const prefix = '/api';

/**
 * Загрузчик файлов bootstrap
 * 
 * @param {array} files Массив файлов
 * 
 * параметры [
 *  multi -- true несколько файлов, false один файл
 *  types -- какие типы файлов загружать
 *  input -- строка для ссылки (true, false),
 *  show url in gallery -- показать ссылку на файл
 * ]
 * 
 */
const _types = {
  img: {
    arr: [
      'image/gif',
      'image/jpeg',
      'image/png'
    ],
    error: 'Вы пытаетесь загрузить запрещенный файл'
  },
  docs: {
    arr: [
      'application/pdf',
      'application/vnd.ms-excel',
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      'application/vnd.ms-powerpoint',
      'application/vnd.openxmlformats-officedocument.presentationml.presentation',
      'application/msword',
      'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
    ],
    error: 'Вы пытаетесь загрузить запрещенный файл'
  },
  all: {
    arr: [
      'image/gif',
      'image/jpeg',
      'image/png',
      'application/pdf',
      'application/vnd.ms-excel',
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      'application/vnd.ms-powerpoint',
      'application/vnd.openxmlformats-officedocument.presentationml.presentation',
      'application/msword',
      'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
    ],
    error: 'Вы пытаетесь загрузить запрещенный файл'
  }
}
export default class FilePicker extends React.Component {
  constructor(props) {
    super(props);
    this.state = ({
      new_files: {
        items: []
      },
      _ID: 'dd'
    })
  }
  change(files) {
    let { _params } = this.props;
    let _arr = Array.from(files),
      new_arr = [],
      _t = _params[1],
      _r = Object.keys(_types);
    if(_r.indexOf(_t) != -1 ) {
      if(_arr.length > 0){
        _arr.forEach((file)=>{
          if(_types[_t].arr.indexOf(file.type) == -1) { // если не найдена
            alert(_types[_t].error || 'проблема в проверке расширения');
          } else new_arr.push(file)
        });
        if(Array.isArray(new_arr) && new_arr.length > 0) {
          this.sendFile(new_arr);
        }
      };
    } else {
      alert('s', _types[_t].error);
      return false;
    }
  }
  sendFile(value, tof) {
    let { config, _params } = this.props;
    if(tof) {
      this.props.onChange(value);
      return false;
    }
    let _arr = Array.from(value);
    if(config) {
      if(config.api) {
        let t = new FormData();
        if(_arr.length > 0){
          _arr.forEach((file)=>{
            t.append(`files`, file);
          })
        }
        _api.post(`${prefix}/${config.api}`, t,{
          s: (ev)=> {
            let _props = update(this.props, {
              files: { $push: ev.data.filesarray || [] }
            });
            this.props.onChange(_props.files);
          },
          e: (er)=> {
            console.log(er);
          }
        });
      } else alert('Ошибка, обратитесь к администратору');
    } else alert('Ошибка, обратитесь к администратору');
  }
  handleDeleteFile(el) {
    let _props = update(this.props, {
      files: { $splice: [[parseInt(el.target.dataset.id), 1]] }
    });
    this.props.onChange(_props.files);
  }
  renderTextInput() {
    let { files, _params } = this.props;
    if(_params[2]) {
      return (
        <div key='fp_1' className="input-group">
          <input
            type='text'
            className='form-control'
            value={files}
            onChange={e=>this.change(e.target.value, true)}
          />
          <span className="input-group-btn">
            <button
              className='btn btn-default'
              onClick={()=>{document.getElementById(this.props._ID).click();}}
            >
              <i className='glyphicon glyphicon-paperclip'/>
            </button>
            <input
              type='file'
              id={this.props._ID}
              className='hidden'
              multiple={_params[0]}
              // onLoadStart={()=>console.log('start')}
              onChange={e=>this.change(e.target.files)}
            />
          </span>
        </div>
      )
    }
  }
  renderButton() {
    let { files, _params } = this.props;
    console.log(files)
    if(_params[2]) {
      return this.renderTextInput();
    } else {
      if(!_params[0]) { // если не мульти то 
        if(Array.isArray(files) && !files.length > 0) {
          return  <div className='row' key='fp_1'>
              <div className='col-md-12'>
                <button
                  className='btn btn-default'
                  onClick={()=>{document.getElementById(this.props._ID).click();}}
                >
                  <i className='glyphicon glyphicon-paperclip'/>
                </button>
                <input
                  type='file'
                  id={this.props._ID}
                  className='hidden'
                  multiple={_params[0]}
                  // onLoadStart={()=>console.log('start')}
                  onChange={e=>this.change(e.target.files)}
                />
              </div>
            </div>
        }
      } else {
        return  <div className='row' key='fp_1'>
            <div className='col-md-12'>
              <button
                className='btn btn-default'
                onClick={()=>{document.getElementById(this.props._ID).click();}}
              >
                <i className='glyphicon glyphicon-paperclip'/>
              </button>
              <input
                type='file'
                id={this.props._ID}
                className='hidden'
                multiple={_params[0]}
                // onLoadStart={()=>console.log('start')}
                onChange={e=>this.change(e.target.files)}
              />
            </div>
          </div>
      }
    }
  }
  render() {
    let { files, _params } = this.props;
    return [
      this.renderButton(),
      <div className='row' key='fpii'>
        <FilesRender
          files={files || []}
          multiple={_params[0]}
          show_url={_params[3]}
          handleDelete={(ev)=>{this.handleDeleteFile(ev)}}
        />
      </div>
    ];
  }
}

const FilesRender = (props) => {
  let { files, multiple, show_url } = props;
  let _class = 'col-sm-12 col-md-12'
  if(multiple) {
    _class = 'col-sm-6 col-md-4'
  }
  if(Array.isArray(files) && files.length > 0) 
      return files.map((el, i)=>{
          if(el.filename.indexOf('.jpg') > 0 || el.filename.indexOf('.jpeg') > 0 || el.filename.indexOf('.png') > 0)
              return (
                  <div key={`${i}filerend`} className={_class}>
                      <div className="thumbnail">
                        <img src={el.filepath} style={{ height: '140px' }}/>
                        <div className="caption">
                          <h5>{ el.filename || 'Ошибка' }</h5>
                          <p style={{color: '#969696', display: show_url ? 'block' : 'none' }}>{ el.filepath || '' }</p>
                          <p>
                            <button
                              data-id={i}
                              onClick={ev=>props.handleDelete(ev)}
                              className='btn btn-danger'
                            >
                              Удалить
                            </button>
                          </p>
                        </div>
                      </div>
                  </div>
              )
          else return (
            <div key={`${i}filerendd`} className={_class}>
              <div className="thumbnail">
                <span className="glyphicon glyphicon-file" style={{
                  fontSize: '50px',
                  width: '100%',
                  textAlign: 'center',
                  height: '140px',
                  top: '50px'
                }}></span>
                <div className="caption">
                  <h5>{ el.filename }</h5>
                  <p style={{color: '#969696', display: show_url ? 'block' : 'none' }}>{ el.filepath || '' }</p>
                  <p>
                    <button
                      data-id={i}
                      onClick={ev=>props.handleDelete(ev)}
                      className='btn btn-danger'
                    >
                      Удалить
                    </button>
                  </p>
                </div>
              </div>
            </div>
          )
      })
    else return null
}