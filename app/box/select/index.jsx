import React from 'react';
import update from 'immutability-helper';
import Select from 'react-select'
import { Axios, LastIndex as LastItem, getBase64 } from '../../utils';
import { randomFillSync } from 'crypto';
let api = new Axios();
const prefix = '/api';

// "react-select": "^2.0.0",

/**
 * select bootstrap
 * 
 * @param {array} files Массив файлов
 * 
 */

 /*
  tag: 'select',
  class: 'form-control',
  item: 'moduleid',
  placeholder: 'Выберите меню',
  type: 'select',
  config: {
    api: 'api/menugroups?pagenum',
    label: 'name'
  },
  disable: false

  -------------------------------------

  options = [
    { value: 1, label: 'Chocolate' },
    { value: 2, label: 'Strawberry' },
    { value: 3, label: 'Vanilla' }
  ];
*/
export default class SelectBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = ({
      data: []
    });
  }
  componentWillMount() {
    let { config } = this.props;
    let arr = [{
      value: 0,
      label: ''
    }];
    let r = new Object;
    if(config.data_params) {
      let k = Object.keys(config.data_params);
      k.map((el, i)=>{
        r[el] = this.props[config.data_params[el]];
      })
    }
    if(config.params) Object.assign(r, config.params);
    api.get(`${prefix}/${config.api}`, r, {
      s: (res) => {
        if(Array.isArray(res.data.outjson) && res.data.outjson.length > 0) {
          res.data.outjson.map((el,i)=>{
            arr.push({
              value: el[config.id],
              label: el[config.label]
            })
          })
        }
        this.setState({
          data: arr || []});
        }
    });
  }
  handleChange = (selectedOption) => {
    this.props.onChange(selectedOption.value);
  }
  render() {
    let { value, placeholder } = this.props;
    let _val = this.state.data.filter((val)=>{
      return val.value === value;
    })
    console.log(_val, value);
    return (
      <Select
        value={_val || {}}
        placeholder={placeholder}
        onChange={this.handleChange}
        options={this.state.data}
      />
    )
  }
}