import React, { Component } from 'react';
import './preloader.css';

export default function Preloader(props){
  if(props.visible)
    return (
      <div className='preloader_block'>
        <div className="loader">
          <div className="loader-bg">
          <span>Загрузка</span>
          </div>
          <div className="drops">
            <div className="drop1"></div>
            <div className="drop2"></div>  
          </div>
        </div>
        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" style={{ display: 'none'}}>
          <defs>
            <filter id="liquid">
              <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
              <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7" result="liquid" />
            </filter>
          </defs>
        </svg>
      </div>
    )
  else return null;
}
