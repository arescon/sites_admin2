import React, { Component } from 'react';
import update from 'immutability-helper';

export default class Modal extends Component {
  constructor(props) {
    super(props);
    this.state = ({
      md_id: this.props.md_id
    })
  }
  handlerClose(id, type){
    if(type === 'success') {
      this.props.handlerSuccess(type);
    } else if(type === 'cancel') {
      this.props.handlerCancel(type);
    }
    $(`#${id}`).modal('hide');
  }
  render() {
    return [
      <button
        type="button"
        className={`btn btn-primary ${this.props.classN || ''}`}
        data-toggle="modal"
        data-target={`#${this.state.md_id}`}
        key='mod_1'
      >
        { this.props.title || <span className="glyphicon glyphicon-cog"></span> }
      </button>,
      <div
        className="modal fade"
        id={this.state.md_id}
        tabIndex="-1"
        role="dialog"
        aria-hidden="true"
        key='mod_2'
      >
        <div className={`modal-dialog ${this.props.class || ''}`} role="document">
          <div className={`modal-content ${ this.props.class === 'full-screen-dialog' ? 'full-screen-content' : '' }`}>
            <div className="modal-header">
              <h5 className="modal-title">{ this.props.header || 'Редактирование'}</h5>
              <button
                type="button"
                className="close"
                aria-label="Close"
                onClick={()=>this.handlerClose(this.state.md_id)}
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
            {
              this.props.children || null
            }
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                onClick={()=>{
                  this.handlerClose(this.state.md_id, 'cancel')}
                }
              >
                { this.props.titleCancel || 'Отменить' }
              </button>
              <button
                type="button"
                className="btn btn-primary"
                onClick={()=>{
                  this.handlerClose(this.state.md_id, 'success')}
                }
              >
                { this.props.titleOk || 'Сохранить' }
              </button>
            </div>
          </div>
        </div>
      </div>
    ]
  }
}