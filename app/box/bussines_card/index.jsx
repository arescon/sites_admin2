import React, { Component } from 'react';
import './bussines_card.css';

export default function Buss_card(props){
  return [
    <section key='buss_card_1' className="bussinesscard">
      <div className="flip">
        <div className="front">				
          <div className="top">				
            <div className="logo">
              <span className="fat">A</span>
              <span className="skinny">J</span>
            </div>				
          </div>
          <div className="nametroduction">
            <div className="name">Alex Joen</div>
            <div className="introduction">Blogger Newbie Asal Sukabumi</div>
          </div>	
          <div className="contact">						
            <div className="website">
              <span className="ion-earth"></span>
              <a href="#">www.alexjoen.web.id</a>
            </div>
            <div className="twitter">
              <span className="ion-social-twitter"></span>
              <a href="#">@alexjoen1</a> 
            </div>												
            <div className="phone ">
              <span className="ion-ios7-telephone"></span> 
              <a href="#">0118 999 7253</a> 
            </div>
            <div className="email ">
              <span className="ion-paper-airplane"></span>
              <a href="#">alexjoen.sukabumi@gmail.com</a>
            </div>
          </div>						
        </div>
        <div className="back"></div>
      </div>
    </section>,
    <section key='buss_card_2' className="tooltip">
      <p>
        Hover over the card to see the back.
      </p>
    </section>
  ]
}