import { connect } from 'react-redux';
import { toast } from 'react-toastify';

import compose from 'recompose/compose';
import lifecycle from 'recompose/lifecycle';
import withState from 'recompose/withState';
import withHandlers from 'recompose/withHandlers';

import { parseJson } from 'app/configurator';
import { changeOne } from 'actions/app';

import { Axios } from 'app/utils';
const api = new Axios();
const prefix = '/api';

export default compose(
  connect(
    state => ({
      one: state.app.one
    }),
    dispatch => ({
      changeOne: (obj) => dispatch(changeOne(obj)),
    })
  ),

  withState('config', 'handlerConfig', []),

  withHandlers({
    handlerSend: ({ one, refresh }) => (e) => {
      e.preventDefault();
      api.post(`${prefix}/settings`, one,{
        s: ()=>{
          toast.success('Сохранено');
          refresh();
        }
      });
    },
    handlerolor: ({ one, changeOne }) => (color) => {
      let _one = {...one};
      _one.color = color.hex;
      changeOne(_one);
    }
  }),

  lifecycle({
    componentDidMount() {
      const { handlerConfig, one } = this.props;
      const f = [
        {
          block: 'div',
          params: {
            css: ['row']
          },
          content: [
            {
              block: 'div',
              params: {
                css: ['col-md-6', 'form-group']
              },
              content: [
                {
                  block: 'label',
                  params: {
                    css: [],
                    val: 'Заголовок сайта'
                  }
                },
                {
                  block: 'input',
                  params: {
                    css: ['form-control'],
                    type: 'text',
                    placeholder: 'Городская поликлинника',
                    val: 'title',
                    def: ''
                  }
                }
              ]
            },
            {
              block: 'div',
              params: {
                css: ['col-md-6', 'form-group']
              },
              content: [
                {
                  block: 'label',
                  params: {
                    css: [],
                    val: 'Слоган'
                  }
                },
                {
                  block: 'input',
                  params: {
                    css: ['form-control'],
                    type: 'text',
                    placeholder: 'Государственное бюджетное учреждение здравоохранения',
                    val: 'slogan',
                    def: ''
                  }
                }
              ]
            },
          ]
        },
        {
          block: 'div',
          params: {
            css: ['row']
          },
          content: [
            {
              block: 'div',
              params: {
                css: ['col-md-6', 'form-group']
              },
              content: [
                {
                  block: 'label',
                  params: {
                    css: [],
                    val: 'Телефон'
                  }
                },
                {
                  block: 'input',
                  params: {
                    css: ['form-control'],
                    type: 'text',
                    placeholder: 'телефон',
                    val: 'phone',
                    def: ''
                  }
                }
              ]
            },
            {
              block: 'div',
              params: {
                css: ['col-md-6', 'form-group']
              },
              content: [
                {
                  block: 'label',
                  params: {
                    css: [],
                    val: 'Эл.Почта'
                  }
                },
                {
                  block: 'input',
                  params: {
                    css: ['form-control'],
                    type: 'text',
                    placeholder: 'example@ex.com',
                    val: 'email',
                    def: ''
                  }
                }
              ]
            }
          ]
        },
        {
          block: 'div',
          params: {
            css: ['row']
          },
          content: [
            {
              block: 'div',
              params: {
                css: ['col-md-12', 'form-group']
              },
              content: [
                {
                  block: 'label',
                  params: {
                    css: [],
                    val: 'Адрес'
                  }
                },
                {
                  block: 'input',
                  params: {
                    css: ['form-control'],
                    type: 'text',
                    placeholder: 'г.Кызыл, ул.Кочетова 1',
                    val: 'address',
                    def: ''
                  }
                }
              ]
            }
          ]
        }
      ];
      let d = parseJson(f);
      handlerConfig(d);
    },
    componentDidUpdate() {
      console.log(this.props.one);
    }
  })
)