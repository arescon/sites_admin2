import React from 'react';
import enhance from './enhance';
import ConfigRender from 'app/configurator/configRender';

import { SketchPicker } from 'react-color'

const RenderFunc = ({
  config, one, changeOne, handlerSend, handlerolor
}) => {
  let _color = one ? one.color || '#fff' : '#fff';
  return (
    <div className='col-md-12'>
      <div className='col-md-9'>
        <ConfigRender
          config={config}
          one={one}
          changeOne={changeOne}
          parent={null}
        />
        <span style={{
          height: '50px',
          width: '100%',
          float: 'left',
          border: '1px solid #ccc',
          background: _color
        }}> цвет </span>
      </div>
      <div className='col-md-3'>
        <SketchPicker
          color={_color}
          onChangeComplete={handlerolor}
        />
      </div>
      <button className='btn btn-success' onClick={handlerSend}>Сохранить изменения</button>
    </div>
  )
}

export default enhance(RenderFunc);
