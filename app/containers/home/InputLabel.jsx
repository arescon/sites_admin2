import React, { Component } from 'react';

export default class LabelInput extends Component {
  constructor(props) {
    super(props);
    this.state = ({
      editable: false
    });
  }
  render() {
    return [
      <div className="form-group" key='sd1'>
        <label>{this.props.title || '*'}</label>
        <span
          className='editable_label'
          style={{display: this.state.editable ? 'none' : 'block'}}
          onClick={(ev)=>{
            this.setState({
              editable: true
            })
          }}>
          {this.props._value || ''}
        </span>
        <input style={{display: this.state.editable ? 'block' : 'none'}}
          value={this.props._value || ''}
          className="form-control"
          onChange={(ev)=>this.props._onChange(ev, this.props.type)}
        />
      </div>
    ]
  }
};
