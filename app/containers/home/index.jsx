import React, { Component } from 'react';
import update from 'immutability-helper';
import md5 from 'md5';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Modal from '../../box/modal/index.jsx';
import RenderConfig from '../../box/renderConfig/index.jsx';

import BasicSettings from './BasicSettings';

import { changeOne } from 'actions/app';

import { Axios } from '../../utils';
let api = new Axios();
const prefix = '/api';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = ({
      slabo: false,
      search: false,
      settings: {
        header: {
          menu: {
            id: 0
          }
        },
        left_column:[],
        right_column:[]
      },
      blocks: []
    });
  }
  componentWillMount() {
    this.getData();
  }
  componentDidMount() {
    $('.dropdown-toggle').dropdown();
  }
  refresh() {
    this.getData();
  }
  getData() {
    api.get(`${prefix}/blocks`, {}, {
      s: (res) => {
        this.setState({
          blocks: res.data.outjson || []
        });
      }
    });
    api.get(`${prefix}/orgsettings`, {}, {
      s: (res) => {
        let p = this.props,
            r = res.data.outjson;
        p.dispatch(changeOne(r))
        this.setState({
          slabo: r.slabo || false,
          search: r.search || false,
        });
      }
    });
  }
  handlerChangeValue(ev, type) {
    this.setState({
      [`${type}`]: ev.target.value
    })
  }
  handlerAddBlock(column, type) {
    api.post(`${prefix}/block`,{
      id: null,
      moduleid: null,
      moduletypeid: type,
      containerid: column
    },{
      s: ()=>{
        this.getData()
      }
    });
  }
  handlerDeleteBlock(el) {
    api.post(`${prefix}/blockdel`,{
      id: el.id
    },{
      s: ()=>{
        this.getData();
      }
    });
  }
  renderBlock(col_id) {
    let arr = this.state.blocks || []
    if(Array.isArray(arr) && arr.length > 0) {
      return arr.map((el, i)=>{
        if(el.containerid == col_id) {
          return (
            <div key={'menu'+col_id+i} style={{ float: 'left', width: '100%' }} >
              <p style={{float: 'left'}}>
                {el.moduletypename}
              </p>
              <button
                style={{float: 'right'}}
                className='btn btn-danger btn-xs'
                onClick={()=>this.handlerDeleteBlock(el)}
              >
                <span className="glyphicon glyphicon-trash"></span>
              </button>
              <RenderModalMenu
                el={el}
                index={i}
                class='btn-xs'
                type={el.moduletypeid}
              />
            </div>
          )
        }
      })
    }
  }
  renderBlockHeader() {
    // id header 6
    let col_id = 6;
    let _arr = this.state.blocks || [];  

    let arr = _arr.filter((item)=>{
      if(item.containerid === 6) return true; else return false;
    });

    if(Array.isArray(arr) && arr.length > 0 ) {
      return arr.map((el, i)=>{
        if(el.containerid == col_id) {
          return (
            <div key={'menu'+col_id+i} style={{ float: 'left', width: '100%' }} >
              <p style={{float: 'left'}}>
                {el.moduletypename}
              </p>
              <button
                style={{float: 'right'}}
                className='btn btn-danger btn-xs'
                onClick={()=>this.handlerDeleteBlock(el)}
              >
                <span className="glyphicon glyphicon-trash"></span>
              </button>
              <RenderModalMenu
                el={el}
                index={i}
                class='btn-xs'
                type={el.moduletypeid}
              />
            </div>
          )
        }
      })
    } else return <button style={{float: 'right'}} onClick={(ev)=>this.handlerAddBlock(6, 1)} className='btn btn-primary'>Настроить</button>
  }
  render() {
    return [
      <div className='row' key='home_1'>
        <div className='col-md-12'>
          <div className="panel panel-primary">
            <div className="panel-heading">
              <h3 className="panel-title">Оповещение системы</h3>
            </div>
            <div className="panel-body">
              Админка и сайты обновились до версии 0.2.3.5.
              <br /><br />
              <span>
                добавлен пункт Обращения граждан
              </span><br />
              <span>
                для получения информации по подключению обращайтесь в jelata
              </span>
              <br /><br />
              ----------------------------
              <br /><br />
              Админка и сайты обновились до версии 0.2.3.1.
              <br /><br />
              <span>
                Обновление редактирования пункта меню
              </span><br />
              <span>
                Можно выбирать публикацию а не вставляет его значение
              </span><br />
              <span>
                Мелкие исправления ошибок
              </span>
              <br /><br />
              ----------------------------
              <br /><br />
              Админка и сайты обновились до версии 0.2.3.0.
              <br /><br />
              <span>
                Обновление блока "Служебная информация"
              </span><br />
              <span>
                Возможность установление цвета сайта (Расширается)
              </span>
            </div>
          </div>
          <div className='page-header'>
            <h1>Настройки <small>Служебная информация</small></h1>
          </div>
          <div className='row'>
            <BasicSettings refresh={()=>this.refresh()} />
          </div>
        </div>
        <div className='col-md-12'>
          <div className='page-header'>
            <h1>Шаблон <small>Общий вид</small></h1>
          </div>
          <div className='row header'>
            <div className='col-md-12'>
              <div className='panel panel-default'>
                <div className='panel-body'>
                  <h3>Шапка сайта</h3>
                  <div className='col-md-4'>
                    <div className='panel panel-default'>
                      <div className='panel-body'>
                        <div id='block_slabo'>
                          <h3><small>Версия для слабовидящих</small></h3>
                          <div className='btn-group' data-toggle='buttons'>
                            <label
                              onClick={()=>{
                                api.post(`${prefix}/togleslabo`,{},{
                                  s: ()=>{
                                    this.setState({
                                      slabo: !this.state.slabo
                                    })
                                  }
                                });
                              }}
                              className={`btn btn-primary ${this.state.slabo ? 'active' : '' }`}
                            >
                              <input
                                type='radio'
                                name='options'
                                autoComplete='off'/>Включить
                            </label>
                            <label
                              onClick={()=>{
                                  api.post(`${prefix}/togleslabo`,{},{
                                    s: ()=>{
                                      this.setState({
                                        slabo: !this.state.slabo
                                      })
                                    }
                                  });
                                }}
                              className={`btn btn-primary ${!this.state.slabo ? 'active' : '' }`}
                            >
                              <input
                                type='radio'
                                name='options'
                                autoComplete='off'/>Выключить
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className='col-md-4'>
                    <div className='panel panel-default'>
                      <div className='panel-body'>
                        <div id='block_search'>
                          <h3><small>Поиск</small></h3>
                          <div className='btn-group' data-toggle='buttons'>
                            <label
                              onClick={()=>{
                                api.post(`${prefix}/toglesearch`,{},{
                                  s: ()=>{
                                    this.setState({
                                      search: !this.state.search
                                    })
                                  }
                                });
                              }}
                              className={`btn btn-primary ${this.state.search ? 'active' : '' }`}
                            >
                              <input
                                type='radio'
                                name='options'
                                autoComplete='off'/>Включить
                            </label>
                            <label
                              onClick={()=>{
                                api.post(`${prefix}/toglesearch`,{},{
                                  s: ()=>{
                                    this.setState({
                                      search: !this.state.search
                                    })
                                  }
                                });
                              }}
                              className={`btn btn-primary ${!this.state.search ? 'active' : '' }`}
                            >
                              <input
                                type='radio'
                                name='options'
                                autoComplete='off'/>Выключить
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='col-md-12'>
              <div className='panel panel-default'>
                <div className='panel-body'>
                  Настройка меню шапки
                  { this.renderBlockHeader() }
                </div>
              </div>
            </div>
          </div>
          <div className='row'>
            <div className='col-md-3'>
              <div className='panel panel-default'>
                <div className='panel-body'>
                  <div className='row'>
                    <div className='col-md-12'>
                      { this.renderBlock(1) }
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-md-12'>
                      <div className="dropup">
                        <button
                          id="dLabel"
                          type="button"
                          className="btn btn-primary dropdown-toggle"
                          data-toggle="dropdown"
                          aria-haspopup="true"
                          aria-expanded="false"
                        >
                          Добавить блок
                          <span className="caret"></span>
                        </button>
                        <ul className="dropdown-menu" aria-labelledby="dLabel">
                          <li className="dropdown-header">Тип блока</li>
                          <li>
                            <a href="#" onClick={(ev)=>this.handlerAddBlock(1, 1)}>Меню</a>
                          </li>
                          <li>
                            <a href="#" onClick={(ev)=>this.handlerAddBlock(1, 2)}>Баннер</a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='col-md-6'>
              <div className='panel panel-default'>
                <div className='panel-body'>
                  content
                </div>
              </div>
            </div>
            <div className='col-md-3'>
              <div className='panel panel-default'>
                <div className='panel-body'>
                  <div className='row'>
                    <div className='col-md-12'>
                      { this.renderBlock(2) }
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col-md-12'>
                      <div className="dropup">
                        <button
                          id="dLabel"
                          type="button"
                          className="btn btn-primary dropdown-toggle"
                          data-toggle="dropdown"
                          aria-haspopup="true"
                          aria-expanded="false"
                        >
                          Добавить блок
                          <span className="caret"></span>
                        </button>
                        <ul className="dropdown-menu" aria-labelledby="dLabel">
                          <li className="dropdown-header">Тип блока</li>
                          <li>
                            <a href="#" onClick={(ev)=>this.handlerAddBlock(2, 1)}>Меню</a>
                          </li>
                          <li>
                            <a href="#" onClick={(ev)=>this.handlerAddBlock(2, 2)}>Баннер</a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='row'>
            <div className='col-md-12'>
              <div className='panel panel-default'>
                <div className='panel-body'>
                  footer
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    ]
  }
}

class RenderModalMenu extends Component {
  constructor(props) {
    super(props);
    this.state = ({

      layout: {
        1: [{
          tag: 'div',
          class: 'col-md-12',
          childs: [
            {
              tag: 'div',
              class: 'row',
              childs: [
                {
                  tag: 'div',
                  class: 'col-md-8 col-xs-8',
                  childs: [
                    {
                      tag: 'div',
                      class: 'form-group',
                      childs: [
                        {
                          tag: 'label',
                          class: '',
                          title: 'Выберите меню'
                        },
                        {
                          tag: 'select',
                          class: 'form-control',
                          item: 'moduleid',
                          placeholder: 'Выберите меню',
                          type: 'select',
                          config: {
                            api: 'menugroups',
                            params: {
                              pagenum: 1,
                              pagesize: 20,
                              substr: ''
                            },
                            label: 'title',
                            id: 'id'
                          },
                          disable: false
                        }
                      ]
                    }
                  ]
                },
                {
                  tag: 'div',
                  class: 'col-md-4 col-xs-4',
                  childs: [
                    {
                      tag: 'div',
                      class: 'form-group',
                      childs: [
                        {
                          tag: 'label',
                          class: '',
                          title: 'список ссылок'
                        },
                        {
                          tag: 'button',
                          type: 'links',
                          url: '/menu_items',
                          item: 'moduleid',
                          source: 'data',
                          class: 'btn btn-info',
                          label: 'Настроить меню',
                          behaviors: {
                            visible: 2, // 1 - true, 0 - false, 2 - if(item) true
                          }
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }],
        2: [{
          tag: 'div',
          class: 'col-md-12',
          childs: [
            {
              tag: 'div',
              class: 'row',
              childs: [
                {
                  tag: 'div',
                  class: 'col-md-8 col-xs-8',
                  childs: [
                    {
                      tag: 'div',
                      class: 'form-group',
                      childs: [
                        {
                          tag: 'label',
                          class: '',
                          title: 'Выберите баннер'
                        },
                        {
                          tag: 'select',
                          class: 'form-control',
                          item: 'moduleid',
                          placeholder: 'Выберите баннер',
                          type: 'select',
                          config: {
                            api: 'banners',
                            params: {
                              pagenum: 1,
                              pagesize: 20,
                              substr: ''
                            },
                            label: 'title',
                            id: 'id'
                          },
                          disable: false
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }]
      },
      data: {}
    })
  }
  handlerChange(item, value) {
    let newState = update(this.state, {
      data: {
        [item]: { $set: value }
      }
    })
    this.setState(newState);
  }
  handlerSuccess(type) {
    api.post(`${prefix}/block`,{
      id: this.props.el.id,
      moduleid: this.state.data.moduleid,
      moduletypeid: this.props.el.moduletypeid,
      containerid: this.props.el.containerid
    },{
      s: ()=>{
        alert('Сохранено');
      }
    });
  }
  handlerCancel(type) {
  }
  componentDidMount() {
    this.setState({
      data: this.props.el || {}
    })
  }
  render() {
    let md_id = md5(Math.random()*(999-1)+1)
    return [
      <Modal
        classN={`f_right ${this.props.class}`}
        md_id= {md_id}
        class_backup='full-screen-dialog'
        handlerSuccess={(type)=>this.handlerSuccess(type)}
        handlerCancel={(type)=>this.handlerCancel(type)}
        handlerDelete={(type)=>this.handlerDelete(type)}
      >
        <RenderConfig
          _state={this.state || {}}
          layout={this.state.layout[this.props.type] || []}
          configs_form={this.state.configs_form || {}}
          initItem={()=>this.getData()}
          modal={md_id}
          handlerChange={(item,value)=>this.handlerChange(item,value)}
          handlerSuccess={(type)=>this.handlerSuccess(type)}
          handlerCancel={(type)=>this.handlerCancel(type)}
          handlerDelete={(type)=>this.handlerDelete(type)}
        />
      </Modal>
    ]
  }
}

Home.propTypes = {
  app: PropTypes.any,
  dispatch: PropTypes.func.isRequired
};

Home.contextTypes = {
  router: PropTypes.shape({
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
      replace: PropTypes.func.isRequired
    }).isRequired,
    staticContext: PropTypes.object
  }).isRequired
};

const mapStateToProps = state => {
  return {
    app: state.app
  };
};

export default connect(mapStateToProps, dispatch => ({ dispatch }))(Home);
/*
  {
    tag: 'modal',
    class: '',
    classN: '',
    form: 'menu_new',
    title: 'Добавить пункт меню',
    childs: [
      {
        tag: 'div',
        class: 'form-group',
        childs: [
          {
            tag: 'label',
            class: '',
            title: 'Название меню'
          },
          {
            tag: 'input',
            class: 'form-control',
            item: 'title',
            form_id: 'menu_new',
            placeholder: 'Введите название',
            type: 'text',
            disable: false
          }
        ]
      },
      {
        tag: 'div',
        class: 'form-group',
        childs: [
          {
            tag: 'label',
            class: '',
            title: 'Ссылка'
          },
          {
            tag: 'input',
            class: 'form-control',
            item: 'url',
            form_id: 'menu_new',
            placeholder: 'Введите url',
            type: 'text',
            disable: false
          }
        ]
      }
    ]
  }
 */

 /*
  {
    tag: 'div',
    class: 'row',
    childs: [
      {
        tag: 'div',
        class: 'col-md-12 col-xs-12',
        childs: [
          {
            tag: 'div',
            class: 'table-responsive',
            childs: [
              {
                tag: 'table',
                class: 'table',
                item: 'items',
                items: [
                  {
                    tag: 'th',
                    class: '',
                    item: 'id',
                    type: 'number',
                    title: '№'
                  },
                  {
                    tag: 'th',
                    class: '',
                    item: 'title',
                    type: 'text',
                    title: 'Название'
                  },
                  {
                    tag: 'th',
                    class: '',
                    item: 'url',
                    type: 'text',
                    title: 'url'
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  }
 
 */