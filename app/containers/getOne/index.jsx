import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link, Route } from 'react-router-dom';
import update from 'immutability-helper';
import merge from 'deepmerge';

import RenderConfig from '../../box/renderConfig/index.jsx';
import { Axios, LastIndex as LastItem } from '../../utils';
import * as formActions from '../../redux/actions/forms.js';

let _api = new Axios();
const prefix = '/api';

class RenderGetOne extends Component {
  constructor(props) {
    super(props);
    this.state = ({
      redirect: false,
      menu: {
        title: 'Список меню',
        descr: `
          список меню
        `,
        breadcrumbs: [],
        layout: [
          {
            tag: 'div',
            class: 'col-md-12',
            childs: [
              {
                tag: 'div',
                class: 'row',
                childs: [
                  {
                    tag: 'div',
                    class: 'col-md-2 col-xs-2',
                    childs: [
                      {
                        tag: 'div',
                        class: 'form-group',
                        childs: [
                          {
                            tag: 'label',
                            class: '',
                            title: 'ID'
                          },
                          {
                            tag: 'input',
                            class: 'form-control',
                            item: 'id',
                            form_id: 'menu',
                            placeholder: '0',
                            type: 'number',
                            disable: true
                          }
                        ]
                      }
                    ]
                  },
                  {
                    tag: 'div',
                    class: 'col-md-10 col-xs-10',
                    childs: [
                      {
                        tag: 'div',
                        class: 'form-group',
                        childs: [
                          {
                            tag: 'label',
                            class: '',
                            title: 'Название меню'
                          },
                          {
                            tag: 'input',
                            class: 'form-control',
                            item: 'title',
                            form_id: 'menu',
                            placeholder: 'Введите название',
                            type: 'text',
                            disable: false
                          }
                        ]
                      }
                    ]
                  },
                  {
                    tag: 'div',
                    class: 'col-md-10 col-xs-10',
                    childs: [
                      {
                        tag: 'button',
                        type: 'link',
                        url: '/menu_items',
                        item: 'id',
                        class: 'btn btn-info',
                        label: 'Список ссылок'
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      },
      menu_one: {
        title: 'Меню',
        descr: `
          Создание и редактирование меню 
        `,
        api: {
          get: 'menugroup',
          post: 'menugroup'
        },
        breadcrumbs: [
          {
            title: 'Список меню',
            url: '/menu'
          }
        ],
        buttons: true,
        layout: [
          {
            tag: 'div',
            class: 'row',
            childs: [
              {
                tag: 'div',
                class: 'col-md-12 col-xs-12',
                childs: [
                  {
                    tag: 'div',
                    class: 'row',
                    childs: [
                      {
                        tag: 'div',
                        class: 'col-md-2 col-xs-2',
                        childs: [
                          {
                            tag: 'div',
                            class: 'form-group',
                            childs: [
                              {
                                tag: 'label',
                                class: '',
                                title: 'ID'
                              },
                              {
                                tag: 'input',
                                class: 'form-control',
                                item: 'id',
                                form_id: 'menu',
                                placeholder: '0',
                                type: 'number',
                                disable: true
                              }
                            ]
                          }
                        ]
                      },
                      {
                        tag: 'div',
                        class: 'col-md-10 col-xs-10',
                        childs: [
                          {
                            tag: 'div',
                            class: 'form-group',
                            childs: [
                              {
                                tag: 'label',
                                class: '',
                                title: 'Название меню'
                              },
                              {
                                tag: 'input',
                                class: 'form-control',
                                item: 'title',
                                form_id: 'menu',
                                placeholder: 'Введите название',
                                type: 'text',
                                disable: false
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  },
                ]
              }
            ]
          }
        ]
      },
      menu_items: {
        title: 'Список ссылок',
        descr: `
          список меню
        `,
        api: {
          get: 'menu',
          post: 'menu',
          delete: 'menudel'
        },
        breadcrumbs: [
          {
            title: 'Меню',
            url: '/menu'
          }
        ],
        buttons: false,
        layout: [
          {
            tag: 'div',
            class: 'row',
            childs: [
              {
                tag: 'div',
                class: 'col-md-12 col-xs-12',
                childs: [
                  {
                    tag: 'button',
                    type: 'links',
                    url: '/menu_item',
                    item: 'id',
                    parent: 'id',
                    class: 'btn btn-info',
                    label: 'Новая ссылка'
                  }
                ]
              },
              {
                tag: 'div',
                class: 'col-md-12 col-xs-12',
                childs: [
                  {
                    tag: 'div',
                    class: 'table',
                    childs: [
                      {
                        tag: 'table',
                        class: 'table',
                        item: null,
                        actions: [
                          {
                            id:0,
                            icon:'glyphicon-pencil',
                            method:'GET',
                            name:'Редактировать',
                            url:'menu_item'
                          },
                          {
                            id:1,
                            icon:'glyphicon-chevron-up',
                            method:'POST',
                            name:'Вверх',
                            url:'menu_item_up'
                          },
                          {
                            id:2,
                            icon:'glyphicon-chevron-down',
                            method:'POST',
                            name:'Вниз',
                            url:'menu_item_down'
                          },
                          {
                            id:3,
                            icon:'glyphicon-trash',
                            method:'DELETE',
                            name:'Удалить',
                            url:'menu_item'
                          }
                        ],
                        items: [
                          {
                            tag: 'th',
                            class: '',
                            item: 'orderby',
                            type: 'number',
                            title: 'Сортировка'
                          },
                          {
                            tag: 'th',
                            class: '',
                            item: 'ptitle',
                            type: 'text',
                            title: 'Название'
                          },
                          {
                            tag: 'th',
                            class: '',
                            item: 'apiurl',
                            type: 'text',
                            title: 'url'
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      },
      menu_item: {
        title: 'Пункт меню',
        descr: `
          Добавление и редактирование пункта меню
        `,
        api: {
          get: 'menuone',
          post: 'menu'
        },
        data_params: {
          menugroupid: 'parent'
        },
        breadcrumbs: [
          {
            title: 'Список меню',
            url: '/menu',
          },
          {
            title: 'Меню',
            url: '/menu_items',
            id: 'parent'
          }
        ],
        buttons: true,
        layout: [
          {
            tag: 'div',
            class: 'row',
            childs: [
              {
                tag: 'div',
                class: 'col-md-12 col-xs-12',
                childs: [
                  {
                    tag: 'div',
                    class: 'row',
                    childs: [
                      {
                        tag: 'div',
                        class: 'col-md-2 col-xs-2',
                        childs: [
                          {
                            tag: 'div',
                            class: 'form-group',
                            childs: [
                              {
                                tag: 'label',
                                class: '',
                                title: 'ID'
                              },
                              {
                                tag: 'input',
                                class: 'form-control',
                                item: 'id',
                                form_id: 'menu',
                                placeholder: '0',
                                type: 'number',
                                disable: true
                              }
                            ]
                          }
                        ]
                      },
                      {
                        tag: 'div',
                        class: 'col-md-10 col-xs-10',
                        childs: [
                          {
                            tag: 'div',
                            class: 'form-group',
                            childs: [
                              {
                                tag: 'label',
                                class: '',
                                title: 'Название меню'
                              },
                              {
                                tag: 'input',
                                class: 'form-control',
                                item: 'ptitle',
                                form_id: 'menu',
                                placeholder: 'Введите название',
                                type: 'text',
                                disable: false
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  },
                ]
              }
            ]
          },
          {
            tag: 'div',
            class: 'row',
            childs: [
              {
                tag: 'div',
                class: 'col-md-12 col-xs-12',
                childs: [
                  {
                    tag: 'div',
                    class: 'row',
                    childs: [
                      {
                        tag: 'div',
                        class: 'col-md-6 col-xs-6',
                        childs: [
                          {
                            tag: 'div',
                            class: 'form-group',
                            childs: [
                              {
                                tag: 'label',
                                class: '',
                                title: 'Ссылка'
                              },
                              {
                                tag: 'select',
                                class: 'form-control',
                                item: 'apiurl',
                                placeholder: 'ссылка',
                                type: 'select',
                                config: {
                                  api: 'articles',
                                  data_params: {
                                    pagenum: 1,
                                    pagesize: 1000
                                  },
                                  label: 'title',
                                  id: 'id'
                                },
                                disable: false
                              }
                            ]
                          }
                        ]
                      },
                      {
                        tag: 'div',
                        class: 'col-md-6 col-xs-6',
                        childs: [
                          {
                            tag: 'div',
                            class: 'form-group',
                            childs: [
                              {
                                tag: 'label',
                                class: '',
                                title: 'Родитель'
                              },
                              {
                                tag: 'select',
                                class: 'form-control',
                                item: 'parentid',
                                placeholder: 'Родитель',
                                type: 'select',
                                config: {
                                  api: 'menu',
                                  data_params: {
                                    id: 'parent'
                                  },
                                  label: 'ptitle',
                                  id: 'id'
                                },
                                disable: false
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  },
                ]
              }
            ]
          }
        ]
      },
      banners: {
        title: 'Баннеры',
        descr: `
          список баннеров
        `,
        api: {
          get: 'banners',
          post: 'banner',
          delete: 'bannerdel'
        },
        layout: [
          {
            tag: 'div',
            class: 'row',
            childs: [
              {
                tag: 'div',
                class: 'col-md-12 col-xs-12',
                childs: [
                  {
                    tag: 'button',
                    type: 'links',
                    url: '/banner',
                    item: 'id',
                    class: 'btn btn-info',
                    label: 'Добавить баннер'
                  }
                ]
              },
              {
                tag: 'div',
                class: 'col-md-12 col-xs-12',
                childs: [
                  {
                    tag: 'div',
                    class: 'table',
                    childs: [
                      {
                        tag: 'table',
                        class: 'table',
                        item: null,
                        actions: [
                          {
                            id:0,
                            icon:'glyphicon-pencil',
                            method:'GET',
                            name:'Редактировать',
                            url:'banner'
                          },
                          {
                            id:1,
                            icon:'glyphicon-trash',
                            method:'DELETE',
                            name:'Удалить',
                            url:'bannerdel'
                          }
                        ],
                        items: [
                          {
                            tag: 'th',
                            class: '',
                            item: 'id',
                            type: 'number',
                            title: '№'
                          },
                          {
                            tag: 'th',
                            class: '',
                            item: 'title',
                            type: 'text',
                            title: 'Название'
                          },
                          {
                            tag: 'th',
                            class: '',
                            item: 'url',
                            type: 'text',
                            title: 'url'
                          },
                          {
                            tag: 'th',
                            class: '',
                            item: 'src',
                            type: 'img',
                            title: 'файл'
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      },
      banner: {
        title: 'Баннер',
        descr: `
          Добавление и редактирование баннера
        `,
        api: {
          get: 'banner',
          post: 'banner'
        },
        breadcrumbs: [
          {
            title: 'Баннеры',
            url: '/banners',
          }
        ],
        buttons: true,
        layout: [
          {
            tag: 'div',
            class: 'row',
            childs: [
              {
                tag: 'div',
                class: 'col-md-12 col-xs-12',
                childs: [
                  {
                    tag: 'div',
                    class: 'row',
                    childs: [
                      {
                        tag: 'div',
                        class: 'col-md-2 col-xs-2',
                        childs: [
                          {
                            tag: 'div',
                            class: 'form-group',
                            childs: [
                              {
                                tag: 'label',
                                class: '',
                                title: 'ID'
                              },
                              {
                                tag: 'input',
                                class: 'form-control',
                                item: 'id',
                                placeholder: '0',
                                type: 'number',
                                disable: true
                              }
                            ]
                          }
                        ]
                      },
                      {
                        tag: 'div',
                        class: 'col-md-10 col-xs-10',
                        childs: [
                          {
                            tag: 'div',
                            class: 'form-group',
                            childs: [
                              {
                                tag: 'label',
                                class: '',
                                title: 'Название'
                              },
                              {
                                tag: 'input',
                                class: 'form-control',
                                item: 'title',
                                placeholder: 'Введите название',
                                type: 'text',
                                disable: false
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  },
                ]
              }
            ]
          },
          {
            tag: 'div',
            class: 'row',
            childs: [
              {
                tag: 'div',
                class: 'col-md-12 col-xs-12',
                childs: [
                  {
                    tag: 'div',
                    class: 'form-group',
                    childs: [
                      {
                        tag: 'label',
                        class: '',
                        title: 'Краткое описание'
                      },
                      {
                        tag: 'input',
                        class: 'form-control',
                        item: 'alt',
                        placeholder: 'Краткое описание',
                        type: 'text'
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            tag: 'div',
            class: 'row',
            childs: [
              {
                tag: 'div',
                class: 'col-md-12 col-xs-12',
                childs: [
                  {
                    tag: 'div',
                    class: 'row',
                    childs: [
                      {
                        tag: 'div',
                        class: 'col-md-6 col-xs-6',
                        childs: [
                          {
                            tag: 'div',
                            class: 'form-group',
                            childs: [
                              {
                                tag: 'label',
                                class: '',
                                title: 'Ссылка'
                              },
                              {
                                tag: 'input',
                                class: 'form-control',
                                item: 'url',
                                placeholder: 'Ссылка',
                                type: 'text',
                                disable: false
                              }
                            ]
                          }
                        ]
                      },
                      {
                        tag: 'div',
                        class: 'col-md-6 col-xs-6',
                        childs: [
                          {
                            tag: 'div',
                            class: 'form-group',
                            childs: [
                              {
                                tag: 'label',
                                class: '',
                                title: 'Рисунок баннера (файл или ссылка на файл)'
                              },
                              {
                                tag: 'file_picker',
                                params: [false, 'img', true, false],
                                item: 'src'
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  },
                ]
              }
            ]
          },
        ]
      },
      pages: {
        title: 'Публикации',
        layout: [
          {
            tag: 'div',
            class: 'col-md-12',
            childs: [
              {
                tag: 'div',
                class: 'row',
                childs: [
                  {
                    tag: 'div',
                    class: 'col-md-2 col-xs-2',
                    childs: [
                      {
                        tag: 'div',
                        class: 'form-group',
                        childs: [
                          {
                            tag: 'label',
                            class: '',
                            title: 'ID'
                          },
                          {
                            tag: 'input',
                            class: 'form-control',
                            item: 'id',
                            form_id: 'menu',
                            placeholder: '0',
                            type: 'number',
                            disable: true
                          }
                        ]
                      }
                    ]
                  },
                  {
                    tag: 'div',
                    class: 'col-md-10 col-xs-10',
                    childs: [
                      {
                        tag: 'div',
                        class: 'form-group',
                        childs: [
                          {
                            tag: 'label',
                            class: '',
                            title: 'Название меню'
                          },
                          {
                            tag: 'input',
                            class: 'form-control',
                            item: 'title',
                            form_id: 'menu',
                            placeholder: 'Введите название',
                            type: 'text',
                            disable: false
                          }
                        ]
                      }
                    ]
                  }
                ]
              },
  
            ]
          }
        ]
      },
      page: {
        title: 'Публикация',
        api: {
          get: 'article',
          post: 'article'
        },
        breadcrumbs: [
          {
            title: 'Публикации',
            url: '/pages'
          }
        ],
        buttons: true,
        layout: [
          {
            tag: 'div',
            class: 'col-md-12',
            childs: [
              {
                tag: 'div',
                class: 'row',
                childs: [
                  {
                    tag: 'div',
                    class: 'col-md-2 col-xs-2',
                    childs: [
                      {
                        tag: 'div',
                        class: 'form-group',
                        childs: [
                          {
                            tag: 'label',
                            class: '',
                            title: 'ID'
                          },
                          {
                            tag: 'input',
                            class: 'form-control',
                            item: 'id',
                            form_id: 'menu',
                            placeholder: '0',
                            type: 'number',
                            disable: true
                          }
                        ]
                      }
                    ]
                  },
                  {
                    tag: 'div',
                    class: 'col-md-10 col-xs-10',
                    childs: [
                      {
                        tag: 'div',
                        class: 'form-group',
                        childs: [
                          {
                            tag: 'label',
                            class: '',
                            title: 'Название меню'
                          },
                          {
                            tag: 'input',
                            class: 'form-control',
                            item: 'title',
                            form_id: 'menu',
                            placeholder: 'Введите название',
                            type: 'text',
                            disable: false
                          }
                        ]
                      }
                    ]
                  }
                ]
              },
              {
                tag: 'div',
                class: 'row',
                childs: [
                  {
                    tag: 'div',
                    class: 'col-md-12 col-xs-12',
                    childs: [
                      {
                        tag: 'div',
                        class: 'form-group',
                        childs: [
                          {
                            tag: 'label',
                            class: '',
                            title: 'Контент'
                          },
                          {
                            tag: 'editor',
                            item: 'content'
                          }
                        ]
                      }
                    ]
                  },
                ]
              },
              {
                tag: 'div',
                class: 'row',
                childs: [
                  {
                    tag: 'div',
                    class: 'col-md-12 col-xs-12',
                    childs: [
                      {
                        tag: 'div',
                        class: 'form-group',
                        childs: [
                          {
                            tag: 'label',
                            class: '',
                            title: 'Файлы'
                          },
                          {
                            tag: 'file_picker',
                            params: [true, 'all', false, true],
                            item: 'afiles'
                          }
                        ]
                      }
                    ]
                  },
                ]
              }
            ]
          }
        ]
      },
      news: {
        title: 'Новость',
        api: {
          get: 'new',
          post: 'news'
        },
        breadcrumbs: [
          {
            title: 'Новости',
            url: '/news'
          }
        ],
        buttons: true,
        layout: [
          {
            tag: 'div',
            class: 'col-md-12',
            childs: [
              {
                tag: 'div',
                class: 'row',
                childs: [
                  {
                    tag: 'div',
                    class: 'col-md-2 col-xs-2',
                    childs: [
                      {
                        tag: 'div',
                        class: 'form-group',
                        childs: [
                          {
                            tag: 'label',
                            class: '',
                            title: 'ID'
                          },
                          {
                            tag: 'input',
                            class: 'form-control',
                            item: 'id',
                            placeholder: '0',
                            type: 'number',
                            disable: true
                          }
                        ]
                      }
                    ]
                  },
                  {
                    tag: 'div',
                    class: 'col-md-10 col-xs-10',
                    childs: [
                      {
                        tag: 'div',
                        class: 'form-group',
                        childs: [
                          {
                            tag: 'label',
                            class: '',
                            title: 'Заголовок'
                          },
                          {
                            tag: 'input',
                            class: 'form-control',
                            item: 'title',
                            placeholder: 'Заголовок',
                            type: 'text',
                            disable: false
                          }
                        ]
                      }
                    ]
                  }
                ]
              },
              {
                tag: 'div',
                class: 'row',
                childs: [
                  {
                    tag: 'div',
                    class: 'col-md-4 col-xs-4',
                    childs: [
                      {
                        tag: 'div',
                        class: 'form-group',
                        childs: [
                          {
                            tag: 'label',
                            class: '',
                            title: 'Обложка'
                          },
                          {
                            tag: 'file_picker',
                            params: [false, 'img', true, false],
                            item: 'photo'
                          }
                        ]
                      }
                    ]
                  },
                  {
                    tag: 'div',
                    class: 'col-md-8 col-xs-8',
                    childs: [
                      {
                        tag: 'div',
                        class: 'form-group',
                        childs: [
                          {
                            tag: 'label',
                            class: '',
                            title: 'Краткое описание'
                          },
                          {
                            tag: 'editor',
                            item: 'descr'
                          }
                        ]
                      }
                    ]
                  },
                ]
              },
              {
                tag: 'div',
                class: 'row',
                childs: [
                  {
                    tag: 'div',
                    class: 'col-md-12 col-xs-12',
                    childs: [
                      {
                        tag: 'div',
                        class: 'form-group',
                        childs: [
                          {
                            tag: 'label',
                            class: '',
                            title: 'Контент'
                          },
                          {
                            tag: 'editor',
                            item: 'content'
                          }
                        ]
                      }
                    ]
                  },
                ]
              },
              {
                tag: 'div',
                class: 'row',
                childs: [
                  {
                    tag: 'div',
                    class: 'col-md-12 col-xs-12',
                    childs: [
                      {
                        tag: 'div',
                        class: 'form-group',
                        childs: [
                          {
                            tag: 'label',
                            class: '',
                            title: 'Фотографии'
                          },
                          {
                            tag: 'file_picker',
                            params: [true, 'img', false, true],
                            item: 'galery'
                          }
                        ]
                      }
                    ]
                  },
                ]
              },
              {
                tag: 'div',
                class: 'row',
                childs: [
                  {
                    tag: 'div',
                    class: 'col-md-12 col-xs-12',
                    childs: [
                      {
                        tag: 'div',
                        class: 'form-group',
                        childs: [
                          {
                            tag: 'label',
                            class: '',
                            title: 'Файлы'
                          },
                          {
                            tag: 'file_picker',
                            params: [true, 'docs', false, true],
                            item: 'afiles'
                          }
                        ]
                      }
                    ]
                  },
                ]
              }
            ]
          }
        ]
      },
      data: {},
      data_params: {}
    });
  }
  componentDidMount() {
    this.getData();
    let p = this.props;
    if(p.match.params.url !== p.forms.bread.curUrl) {
      p.dispatch(formActions.BreadChange({
        bread: update(p.forms.bread, {
          curUrl: { $set: p.match.params.url }
        })
      }));
    }
  }
  componentDidUpdate(prevProps) {
    let _url = this.props.match.params.url;
    if (prevProps.match.params.url !== _url) {
      this.getData()
    }
  }
  getData() {
    let url = this.props.match.params.url,
      _id = this.props.match.params.id;
    _api.get(`${prefix}/${this.state[url].api.get}`, {
      id: _id || 0
    }, {
      s: (res) => {
        this.setState({
          data: res.data.outjson || []
        })
      }
    });
  }
  handlerChange(item, value) {
    this.setState({
      data: update(this.state.data, {
        [`${item}`]: { $set: value }
      })
    })
  }
  handlerChangeEdit(item, value) {
    this.setState(update(this.state, { new: { [item]: { $set: value } } }));
  }
  handlerSuccess(ev, history) {
    let url = this.props.match.params.url;
    let merge_obj;
    let d_params = this.state[url].data_params || {},
      arr = Object.keys(d_params),
      arr2 = new Object();
    arr.map((el)=>{
      arr2[el] = this.props.match.params[d_params[el]] || null;
    })
    merge_obj = merge(this.state.data, arr2);
    let _url, _id, _toGo = '';
    if(this.state[url].breadcrumbs) {
      _url = this.state[url].breadcrumbs[this.state[url].breadcrumbs.length-1].url;
      _toGo = _toGo + _url;
    }
    if(this.props.match.params.parent) {
      _id = this.props.match.params[this.state[url].breadcrumbs[this.state[url].breadcrumbs.length-1].id];
      _toGo = _toGo + '/' + _id
    }
    _api.post(`${prefix}/${this.state[url].api.post}`, merge_obj || {}, {
      s: (res)=>{
        history.push(_toGo);
      },
      e: (er)=>{
        console.log(er);
      }
    });
  }
  handlerCancel(ev, history) {
    let _url, _id, _toGo = '';
    let url = this.props.match.params.url;
    if(this.state[url].breadcrumbs) {
      _url = this.state[url].breadcrumbs[this.state[url].breadcrumbs.length-1].url;
      _toGo = _toGo + _url;
    }
    if(this.props.match.params.parent) {
      _id = this.props.match.params[this.state[url].breadcrumbs[this.state[url].breadcrumbs.length-1].id];
      _toGo = _toGo + '/' + _id
    }
    history.push(_toGo);
  }
  handlerActions(el) {
    let url = this.props.match.params.url,
      action = JSON.parse(el.currentTarget.dataset.el),
      id = el.currentTarget.dataset.id;
    switch (action.method) {
        case 'POST':
          _api.post(`${prefix}/${action.url}`,{
            id: id
          },{
            s: ()=>{
              this.getData()
            }
          });
          break;
        case 'PUT':
          _api.put(`${prefix}/${action.url}`, {
            id: id
          },{
            s: ()=>{
              this.getData()
            }
          });
          break;
        case 'DELETE': {
          let _method = this.state[url].api.delete;
          _api.post(`${prefix}/${_method}`, {
              id: id
            },{
              s: ()=>{
                this.getData()
              }
            });
          break;
        };
    }
  }
  handlerEditorChange(item, value) {
    let newState = update(this.state, {
      new: {
        [item]: { $set: value }
      }
    })
    this.setState(newState);
  }
  breadcrumbs(id, url) {
    if(this.state[url].breadcrumbs)
      return this.state[url].breadcrumbs.map((el,i)=>{
        if(i===0){
          return (
            <li key={`l_xleb_${i}`}>
              <Link to={el.url || '/'}>{el.title || ''}</Link>
            </li>
          )
        } else {
          return (
            <li key={`l_xleb_${i}`}>
              <Link to={`${el.url || '/'}/${this.props.match.params[el.id]}`}>{el.title || ''}</Link>
            </li>
          )
        }
      })
  }
  render() {
    let url = this.props.match.params.url,
        parent = this.props.match.params.parent || null,
        id = this.props.match.params.id || null;
    return [
      <div className='row' key='pages_1'>
        <div className='col-md-12'>
          <div className='page-header'>
            <h1>{this.state[url] ? this.state[url].title : 'Страница не существует'} <small>{ this.state[url] ? id === 0 ? 'Создание' : 'Редактирование' : '' }</small></h1>
          </div>
        </div>
        <div className='col-md-12'>
          <ol className='breadcrumb'>
            {
              this.state[url] ? this.breadcrumbs(id, url) : null
            }
            <li className='active'>{ this.state[url] ? this.state[url].title : null}</li>
          </ol>
        </div>
        <div className='col-md-12'>
          <RenderConfig
            _state={this.state || {}}
            layout={this.state[url] ? this.state[url].layout || [] : []}
            url={url}
            id={id}
            parent={parent}
            initItem={()=>this.getData()}
            handlerChange={(item,value)=>this.handlerChange(item,value)}
            handlerSuccess={(type)=>this.handlerSuccess(type)}
            handlerCancel={(type)=>this.handlerCancel(type)}
            handlerDelete={(type)=>this.handlerDelete(type)}
            handlerActions={(el)=>this.handlerActions(el)}
          />
        </div>
        <div className='col-md-12'>
          {
            (this.state[url].buttons) ? <div className='modal-footer'>
                <Button label='Отмена' classN='btn btn-default' func={(ev, history)=>this.handlerCancel(ev, history)} />
                <Button label='Сохранить' classN='btn btn-success' func={(ev, history)=>this.handlerSuccess(ev, history)} />
              </div> : null
          }
          
        </div>
      </div>
    ]
  }
}

const Button = (props) => (
  <Route render={({ history}) => (
    <button
      type='button'
      className={props.classN || ''}
      onClick={(ev)=>props.func(ev, history)}
    >
      { props.label }
    </button>
  )} />
)

class GetOne extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return  <RenderGetOne
              forms = {this.props.forms}
              dispatch = {this.props.dispatch}
              match = {this.props.match}
            />
  }
}

GetOne.propTypes = {
  forms: PropTypes.any,
  dispatch: PropTypes.func.isRequired
};

GetOne.contextTypes = {
  router: PropTypes.shape({
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
      replace: PropTypes.func.isRequired
    }).isRequired,
    staticContext: PropTypes.object
  }).isRequired
};

const mapStateToProps = state => {
  return {
    forms: state.forms
  };
};

export default connect(mapStateToProps, dispatch => ({ dispatch }))(GetOne);
