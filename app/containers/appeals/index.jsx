import React, { Component } from 'react';
import Pagination from 'rc-pagination';
import update from 'immutability-helper';
import { Link } from 'react-router-dom';
import Table from '../../box/table/index.jsx';
import Modal from '../../box/modal/index.jsx';
import RenderConfig from '../../box/renderConfig/index.jsx';

import { Axios } from '../../utils';
let api = new Axios();
const prefix = '/api';

export default class Appeals extends Component {
  constructor(props) {
    super(props);
    this.state = ({
      table: 's',
      config: [
        {
          tag: 'th',
          class: '',
          item: 'id',
          type: 'number',
          title: '№'
        },
        {
          tag: 'th',
          class: '',
          item: 'appealText',
          type: 'text',
          title: 'Обращение'
        },
        {
          tag: 'th',
          class: '',
          item: 'status',
          type: 'text',
          title: 'Статус'
        }
      ],
      pagination: {
        pagenum: 1,
        pagesize: 20,
        foundcount: 0,
        substr: ''
      },
      data: [],
      one: false
    });
  }
  componentWillMount() {
    this.getData(this.state.pagination.pagenum);
  }
  handlerChangePage(page){
    this.getData(page);
    this.setState({
      pagination: update(this.state.pagination,
        {
          ['pagenum']: { $set: page }
        }
      )
    })
  }
  getData(page, substr) {
    api.get(`${prefix}/appeal`, {
      pagenum: page || this.state.pagination.pagenum,
      pagesize: this.state.pagination.pagesize,
      substr: substr || ''
    }, {
      s: (res) => {
        let _res = {...res.data};
        let arr = [];
        res.data.outjson.forEach((el,i)=>{
          arr[i] = Object.assign(res.data.outjson[i], el.a_data)
          arr[i].a_data = {};
          arr[i].status = arr[i].isprocessed ? 'Исполнено' : 'Обрабатывается'
        })
        this.setState({
          data: arr || []
        });
      }
    });
  }
  clickFunc(el) {
    this.setState({
      one: el
    })
  }
  handlerActions(el) {
    let action = JSON.parse(el.currentTarget.dataset.el);
    let id = el.currentTarget.dataset.id;
    let func_name = el.currentTarget.dataset.func_name;
    switch (action.method) {
      case 'POST':
        api.post(`${prefix}/${action.url}`,{
          id: id
        },{
          s: ()=>{
            this.getData(this.state.pagination.pagenum);
          }
        });
        break;
      case 'PUT':
        api.put(`${prefix}/${action.url}?id=${id}`,{},{
          s: ()=>{
            this.getData(this.state.pagination.pagenum);
          }
        });
        break;
      case 'DELETE': {
        console.log('delete');
        api.delete(`${prefix}/${action.url}?id=${id}`,{
            s: ()=>{
              this.getData(this.state.pagination.pagenum);
            }
        });
        break;
      };
    }
  }
  renderOne() {
    console.log(this.state.one);
    let { one } = this.state;
    return [
      <div key='one_s_1' className="col-md-4">
        <h5>От кого:</h5> { (one.firstName || '') } {(one.lastName || '')} {(one.middleName || '') }
      </div>,
      <div key='one_s_2' className="col-md-4">
        <h5>Кому:</h5> { one.recipient || 'Вопрос общей категории'}
      </div>,
      <div key='one_s_3' className="col-md-4">
        <h5>Дата:</h5> { one.created || ''}
      </div>,
      <div key='one_s_4' className="col-md-8">
        <div style={{background: '#ececec'}}>
          <h5>Вопрос:</h5>{ one.appealText || ''}
        </div>
      </div>,
      <div key='one_s_5' className="col-md-4">
        <h5>Файлы:</h5>
        <ol className='list-counter-square'>
          {
            one.files ? (()=>{
                return one.files.map((file, i_f) => {
                  return <li key={'file_'+i_f}><a target='blank' href={file.filepath}>{file.filename}</a></li>
                })
              })() : 'файлов нет'
          }
        </ol>
      </div>
    ]
  }
  render() {
    return [
      <div className="" key='menus_1'>
        <div className="col-md-12">
          <div className="page-header">
            <h1>Обращения граждан<small>список</small></h1>
          </div>
        </div>
        <div className="col-md-12">
          {
            this.state.one ? this.renderOne() : null
          }
        </div>
        <div className="col-md-12">
          <Pagination
            className='ant-pagination'
            style={{margin: '4px 0'}}
            current={ this.state.pagination.pagenum }
            defaultPageSize={ this.state.pagination.pagesize }
            total={ this.state.pagination.foundcount }
            onChange={ (page)=> this.handlerChangePage(page) }
            locale={{ items_per_page: '/странице',
                jump_to: 'Перейти', page: '',
                prev_page: 'Назад', next_page: 'Вперед',
                prev_5: 'Предыдущие 5', next_5: 'Следующие 5'}}
          />
        </div>
        <div className="col-md-12">
          <Table
            _config={this.state.config}
            _data={this.state.data}
            clickFunc={el => this.clickFunc(el)}
            actionFunc={(el)=> this.handlerActions(el)}
            actions={[
              {
                id:1,
                icon:"glyphicon-flash",
                method:"POST",
                name:"Исполнено / обрабатывается",
                url:"appealtogle"
              }
            ]}
          />
        </div>
      </div>
    ]
  }
};