import React, { Component } from 'react';
import update from 'immutability-helper';

import Modal from '../../box/modal/index.jsx';

import Users from './users.jsx';

import SelectBox from '../../box/select/index.jsx';

import { Axios } from '../../utils';
let api = new Axios();
const prefix = '/api';

export default class User extends Component {
  constructor(props) {
    super(props);
    this.state = ({
      data: {
        org: null
      }
    })
  }
  handlerChange(item, value) {
    let newState = update(this.state, {
      data: {
        [item]: { $set: value }
      }
    })
    this.setState(newState);
  }
  sendFrom() {
    api.post(`${prefix}/changeorg`,{
      orgid: this.state.data.org,
    },{
      s: ()=>{
        alert('Сохранено');
      }
    });
  }
  render() {
    return [
      <div className='row' key='user_1'>
        <div className='col-md-12'>
          <div className='page-header'>
            <h1>Настройки <small>пользователя</small></h1>
          </div>
        </div>
      </div>,
      <div className='row' key='user_2'>
        <div className='col-md-6'>
          <div className="panel panel-primary">
            <div className="panel-heading">
              Смена организации
            </div>
            <div className="panel-body">
              <div className="form-group">
                <div className='row'>
                  <div className='col-md-8'>
                    <label>Организация</label>
                    <SelectBox
                      url={this.props.url}
                      id={null}
                      parent={null}
                      className='form-control'
                      value={ this.state.data.org }
                      placeholder='Организация'
                      config={{
                        api: 'dicsel',
                        params: {
                          tagname: 'orgs',
                        },
                        label: 'orgname',
                        id: 'id'
                      }}
                      onChange={(selected)=>{
                        this.handlerChange('org' || null, selected)
                      }}
                    />
                  </div>
                  <div className='col-md-4'>
                    <label style={{ color: 'white' }}>.</label>
                    <button
                      className='btn btn-success form-control'
                      onClick={()=>this.sendFrom()}
                    >
                      Сменить
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='col-md-12'>
          <div className='panel panel-primary'>
            <div className="panel-heading">
              Список пользователей
            </div>
            <div className="panel-body">
              <Users {...this.props} />
            </div>
          </div>
        </div>
      </div>
    ];
  }
}