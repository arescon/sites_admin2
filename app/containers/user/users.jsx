import React, { Component } from 'react';
import Pagination from 'rc-pagination';
import { Link } from 'react-router-dom';
import Table from '../../box/table/index.jsx';

import { Axios } from '../../utils';
let api = new Axios();
const prefix = '/api';

export default class Users extends Component {
  constructor(props){
    super(props);
    this.state = ({
      table: 's',
      config: [
        {
          tag: 'th',
          class: '',
          item: 'fam',
          type: 'text',
          title: 'Фамилия'
        },
        {
          tag: 'th',
          class: '',
          item: 'im',
          type: 'text',
          title: 'Имя'
        },
        {
          tag: 'th',
          class: '',
          item: 'ot',
          type: 'text',
          title: 'Отчество'
        },
        {
          tag: 'th',
          class: '',
          item: 'phone',
          type: 'number',
          title: 'Логин'
        }
      ],
      pagination: {
        isactiv: false,
        pagenum: 1,
        pagesize: 20,
        foundcount: 0,
        substr: ''
      },
      data: []
    });
  }
  componentWillMount() {
    this.getData(this.state.pagination.pagenum);
  }
  getData(page, substr) {
    let url = this.props.match.params.url;
    api.get(`${prefix}/users`, {
      isactiv: this.state.pagination.isactiv,
      pagenum: page || this.state.pagination.pagenum,
      pagesize: this.state.pagination.pagesize,
      substr: substr || ''
    }, {
      s: (res) => {
        let arr = [];
        res.data.outjson.map((el)=>{
          if(el.parentid === 0){
            arr.append(el);
          };
        });
        let st = {...this.state};
        st.data = res.data.outjson || [];
        st.pagination.foundcount = res.data.foundcount || 0
        this.setState(st);
      }
    });
  }
  handlerChangePage(page){
    this.getData(page);
    this.setState({
      pagination: update(this.state.pagination,
        {
          ['pagenum']: { $set: page }
        }
      )
    })
  }
  handlerActions(el) {
    let action = JSON.parse(el.currentTarget.dataset.el);
    let id = el.currentTarget.dataset.id;
    let func_name = el.currentTarget.dataset.func_name;
    switch (action.method) {
      case 'POST':
        api.post(`${prefix}/${action.url}`,{
          id: id
        },{
          s: ()=>{
            this.getData(this.state.pagination.pagenum);
          }
        });
        break;
      case 'PUT':
        api.put(`${prefix}/${action.url}?id=${id}`,{},{
          s: ()=>{
            this.getData(this.state.pagination.pagenum);
          }
        });
        break;
      case 'DELETE': {
        api.delete(`${prefix}/${action.url}?id=${id}`,{
          s: ()=>{
            this.getData(this.state.pagination.pagenum);
          }
        });
        break;
      };
    }
  }
  render(){
    console.log(this.state.data);
    return [
      <div className="" key='pages_1'>
        <div className="col-md-12">
          <div className="form-group col-md-6">
            <div className='form-check'>
              <input
                className='form-check-input'
                checked={this.state.pagination.isactiv}
                onChange={()=>{
                  let _state = {...this.state.pagination};
                  _state.isactiv = !_state.isactiv;
                  this.setState({
                    pagination: _state
                  })
                }}
                type="checkbox"
                id="1"
              />
              <label className="form-check-label">
                Активные
              </label>
            </div>
          </div>
          <div className='col-md-6'>
            <button onClick={()=>{
              this.getData(this.state.pagination.pagenum);
            }}>
              Применить
            </button>
          </div>
        </div>
        <div className="col-md-12">
          <Pagination
            className='ant-pagination'
            style={{margin: '4px 0'}}
            current={ this.state.pagination.pagenum }
            defaultPageSize={ this.state.pagination.pagesize }
            total={ this.state.pagination.foundcount }
            onChange={ (page)=> this.handlerChangePage(page) }
            locale={{ items_per_page: '/странице',
                jump_to: 'Перейти', page: '',
                prev_page: 'Назад', next_page: 'Вперед',
                prev_5: 'Предыдущие 5', next_5: 'Следующие 5'}}
          />
        </div>
        <div className="col-md-12">
          <Table
            _config={this.state.config}
            _data={this.state.data}
            actionFunc={(el)=> this.handlerActions(el)}
            actions={[
              {
                id:0,
                icon:"glyphicon-pencil",
                method:"POST",
                name:"Активировать/ деактивировать",
                url:"user"
              },
              {
                id:2,
                icon:"glyphicon-trash",
                method:"POST",
                name:"Удалить",
                url:"articledel"
              }
            ]}
          />
        </div>
      </div>
    ]
  }
}