import React, { Component } from 'react';
import Pagination from 'rc-pagination';
import update from 'immutability-helper';
import { Link } from 'react-router-dom';
import Table from '../../box/table/index.jsx';

import { Axios } from '../../utils';
let api = new Axios();

export default class Menu_items extends Component {
  constructor(props) {
    super(props);
    this.state = ({
      data: [],
      breadcrumbs: [
        {
          title: 'Меню',
          url: '/menu'
        }
      ],
    });
  }
  componentDidMount() {
    this.getData();
  }
  getData() {
    let id = this.props.match.params.id;
    api.get('/api/menu', {
      id: id
    }, {
      s: (res) => {
        this.setState({
          data: res.data.outjson || []
        });
      }
    });
  }
  orderBy(id, up) {
    api.post('/api/upordown', {
      id: id,
      up: up
    }, {
      s: () => {
        this.getData();
      }
    });
  }
  delete(id) {
    api.post('/api/menudel', {
      id: id
    }, {
      s: () => {
        this.getData();
      }
    });
  }
  breadcrumbs() {
    if(this.state.breadcrumbs)
      return this.state.breadcrumbs.map((el,i)=>{
        if(i===0){
          return (
            <li key={`l_xleb_${i}`}>
              <Link to={el.url || '/'}>{el.title || ''}</Link>
            </li>
          )
        } else {
          return (
            <li key={`l_xleb_${i}`}>
              <Link to={`${el.url || '/'}/${this.props.match.params[el.id]}`}>{el.title || ''}</Link>
            </li>
          )
        }
      })
  }
  render() {
    let id = this.props.match.params.id;
    return [
      <div className="" key='menus_1'>
        <div className="col-md-12">
          <div className="page-header">
            <h1>Ссылки<small>список</small></h1>
          </div>
        </div>
        <div className='col-md-12'>
          <ol className='breadcrumb'>
            { this.breadcrumbs() }
            <li className='active'>Список ссылок меню</li>
          </ol>
        </div>
        <div className='col-md-12'>
          <Link to={`/menu_item/${id}/0`} className='btn btn-primary'>
            Новая ссылка
          </Link>
        </div>
        <div className="col-md-12">
          <ListBlock
            id_menu={id}
            marginl={0}
            parent={0}
            list={this.state.data || []}
            label='ptitle'
            descr='orderby'
            orderBy={(id,up)=>this.orderBy(id,up)}
            delete={(id)=>this.delete(id)}
          />
        </div>
      </div>
    ]
  }
}

class ListBlock extends Component {
  constructor(props){
    super(props);
  }
  renderChild(el, items) {
    if(el.childcount > 0) 
      return <ListBlock
              id_menu={this.props.id_menu}
              marginl={this.props.marginl + 15}
              parent={el.id} list={items || []}
              label={this.props.label}
              descr={this.props.descr}
              orderBy={(id,up)=>this.props.orderBy(id,up)}
              delete={(id)=>this.props.delete(id)}
            />
  }
  render_li(items) {
    let p = this.props;
    if(items.length > 0) {
      let _arr = items.filter(item => item.parentid === this.props.parent);
      return _arr.map((el, i)=>{
        return (
          <li key={`ss_${i}`} className="list-group-item" style={{
            borderRight: 'none',
            borderLeft: 'none',
            padding: '10px 0px'
          }}>
            <div className='' style={{float: 'right', marginTop: '-5px'}}>
              <div className="btn-group btn-group-sm" style={{marginRight: '10px'}} role="group" aria-label="...">
                <button
                  type="button"
                  className="btn btn-default"
                  onClick={()=>this.props.orderBy(el.id,true)}
                >
                  <span className='glyphicon glyphicon-chevron-up'></span>
                </button>
                <button
                  type="button"
                  className="btn btn-default"
                  onClick={()=>this.props.orderBy(el.id,false)}
                >
                  <span className='glyphicon glyphicon-chevron-down'></span>
                </button>
              </div>
              <div className="btn-group btn-group-sm" role="group" aria-label="...">
                <Link to={`/menu_item/${p.id_menu}/${el.id}`} className='btn btn-info'>
                  <span className='glyphicon glyphicon-pencil'></span>
                </Link>
                <button
                  type="button"
                  className="btn btn-danger"
                  onClick={()=>this.props.delete(el.id)}
                >
                  <span className='glyphicon glyphicon-trash'></span>
                </button>
              </div>
            </div>
            <p style={{margin: '0'}}>
              {`${el[p.descr]} - ${el[p.label]}`}
            </p>
            { this.renderChild(el, items) }
          </li>
        ) 
      })
    } else return null;
  }
  render() {
    return (
      <ul className="list-group" style={{
        marginTop: '10px',
        paddingLeft: this.props.marginl || 0,
        marginBottom: '-11px'
      }}>
        { this.render_li(this.props.list) }
      </ul>
    );
  }
}