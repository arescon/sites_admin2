import React, { Component } from 'react';
import Pagination from 'rc-pagination';
import update from 'immutability-helper';
import { Link } from 'react-router-dom';
import Table from '../../box/table/index.jsx';
import Modal from '../../box/modal/index.jsx';
import RenderConfig from '../../box/renderConfig/index.jsx';

import { Axios } from '../../utils';
let api = new Axios();
const prefix = '/api';

export default class Menus extends Component {
  constructor(props) {
    super(props);
    this.state = ({
      table: 's',
      config: [
        {
          tag: 'th',
          class: '',
          item: 'id',
          type: 'number',
          title: '№'
        },
        {
          tag: 'th',
          class: '',
          item: 'title',
          type: 'text',
          title: 'Название'
        }
      ],
      pagination: {
        pagenum: 1,
        pagesize: 20,
        foundcount: 0,
        substr: ''
      },
      data: []
    });
  }
  componentWillMount() {
    this.getData(this.state.pagination.pagenum);
  }
  handlerChangePage(page){
    this.getData(page);
    this.setState({
      pagination: update(this.state.pagination,
        {
          ['pagenum']: { $set: page }
        }
      )
    })
  }
  getData(page, substr) {
    let url = this.props.match.params.url;
    api.get(`${prefix}/menugroups`, {
      pagenum: page || this.state.pagination.pagenum,
      pagesize: this.state.pagination.pagesize,
      substr: substr || ''
    }, {
      s: (res) => {
        let arr = [];
        res.data.outjson.map((el)=>{
          if(el.parentid === 0){
            arr.append(el);
          }
        })
        this.setState({
          data: res.data.outjson || []
        })
      }
    });
  }
  handlerActions(el) {
    let action = JSON.parse(el.currentTarget.dataset.el);
    let id = el.currentTarget.dataset.id;
    let func_name = el.currentTarget.dataset.func_name;
    switch (action.method) {
      case 'POST':
        api.post(`${prefix}/${action.url}`,{
          id: id
        },{
          s: ()=>{
            this.getData(this.state.pagination.pagenum);
          }
        });
        break;
      case 'PUT':
        api.put(`${prefix}/${action.url}?id=${id}`,{},{
          s: ()=>{
            this.getData(this.state.pagination.pagenum);
          }
        });
        break;
      case 'DELETE': {
        console.log('delete');
        api.delete(`${prefix}/${action.url}?id=${id}`,{
            s: ()=>{
              this.getData(this.state.pagination.pagenum);
            }
        });
        break;
      };
    }
  }
  render() {
    return [
      <div className="" key='menus_1'>
        <div className="col-md-12">
          <div className="page-header">
            <h1>Меню<small>список</small></h1>
          </div>
        </div>
        <Link to='menu_one/0' className='btn btn-primary'>
          Новое меню
        </Link>
        <div className="col-md-12">
          <Pagination
            className='ant-pagination'
            style={{margin: '4px 0'}}
            current={ this.state.pagination.pagenum }
            defaultPageSize={ this.state.pagination.pagesize }
            total={ this.state.pagination.foundcount }
            onChange={ (page)=> this.handlerChangePage(page) }
            locale={{ items_per_page: '/странице',
                jump_to: 'Перейти', page: '',
                prev_page: 'Назад', next_page: 'Вперед',
                prev_5: 'Предыдущие 5', next_5: 'Следующие 5'}}
          />
        </div>
        <div className="col-md-12">
          <Table
            _config={this.state.config}
            _data={this.state.data}
            actionFunc={(el)=> this.handlerActions(el)}
            actions={[
              {
                id:0,
                icon:"glyphicon-pencil",
                method:"GET",
                name:"Редактировать",
                url:"menu_one"
              },
              {
                id:1,
                icon:"glyphicon-th-list",
                method:"GET",
                name:"Список ссылок",
                url:"menu_items"
              },
              {
                id:2,
                icon:"glyphicon-trash",
                method:"POST",
                name:"Удалить",
                url:"menugroupdel"
              }
            ]}
          />
        </div>
      </div>
    ]
  }
}

/*
  {
    id: 0,
    title: "О чем то",
    url: "/page/about",
    order_by: 1,
    parentid: null,
    childcount: 0
  }
*/