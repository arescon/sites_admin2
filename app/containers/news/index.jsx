import React, { Component } from 'react';
import Pagination from 'rc-pagination';
import { Link } from 'react-router-dom';
import Table from '../../box/table/index.jsx';

import { Axios } from '../../utils';
let api = new Axios();
const prefix = '/api';

export default class NewsIndex extends Component {
  constructor(props){
    super(props);
    this.state = ({
      table: 's',
      config: [
        {
          tag: 'th',
          class: '',
          item: 'id',
          type: 'number',
          title: '№'
        },
        {
          tag: 'th',
          class: '',
          item: 'title',
          type: 'text',
          title: 'Название'
        },
        {
          tag: 'th',
          class: '',
          item: 'tags',
          type: 'array',
          title: 'Теги'
        },
        {
          tag: 'th',
          class: '',
          item: 'created',
          type: 'datetime',
          title: 'Дата создания'
        },
      ],
      pagination: {
        pagenum: 1,
        pagesize: 10,
        foundcount: 0,
        substr: ''
      },
      data: []
    });
  }
  componentWillMount() {
    this.getData(this.state.pagination.pagenum);
  }
  componentDidUpdate() {
    console.log(this.state.pagination);
  }
  getData(page, substr) {
    let url = this.props.match.params.url;
    api.get(`${prefix}/news`, {
      pagenum: page || this.state.pagination.pagenum,
      pagesize: this.state.pagination.pagesize,
      substr: substr || ''
    }, {
      s: (res) => {
        let arr = [];
        res.data.outjson.map((el)=>{
          if(el.parentid === 0){
            arr.append(el);
          }
        })
        let st = {...this.state};
        st.data = res.data.outjson || [];
        st.pagination.foundcount = res.data.foundcount || 0
        this.setState(st);
      }
    });
  }
  handlerChangePage(page){
    this.getData(page);
    this.setState({
      pagination: update(this.state.pagination,
        {
          ['pagenum']: { $set: page }
        }
      )
    })
  }
  handlerActions(el) {
    let action = JSON.parse(el.currentTarget.dataset.el);
    let id = el.currentTarget.dataset.id;
    let func_name = el.currentTarget.dataset.func_name;
    switch (action.method) {
        case 'POST':
            if(action.id === 99) {
              const conf = confirm(`Вы действительно хотите удалить?`);
              if(conf) {
                api.post(`${prefix}/${action.url}`,{
                  id: id
                },{
                  s: ()=>{
                    this.getData(this.state.pagination.pagenum);
                  }
                });
              }
            } else {
              api.post(`${prefix}/${action.url}`,{
                id: id
              },{
                s: ()=>{
                  this.getData(this.state.pagination.pagenum);
                }
              });
            };
            break;
        case 'PUT':
            api.put(`${prefix}/${action.url}?id=${id}`,{},{
              s: ()=>{
                this.getData(this.state.pagination.pagenum);
              }
            });
            break;
    }
  }
  render() {
    return [
      <div className="" key='pages_1'>
        <div className="col-md-12">
          <div className="page-header">
            <h1>Новости <small>список</small></h1>
          </div>
        </div>
        <Link to='news/0' className='btn btn-primary'>
          Добавить новость
        </Link>
        <div className="col-md-12">
          <Pagination
            className='ant-pagination'
            style={{margin: '4px 0'}}
            current={ this.state.pagination.pagenum }
            defaultPageSize={ this.state.pagination.pagesize }
            total={ this.state.pagination.foundcount }
            onChange={ (page)=> this.handlerChangePage(page) }
            locale={{ items_per_page: '/странице',
                jump_to: 'Перейти', page: '',
                prev_page: 'Назад', next_page: 'Вперед',
                prev_5: 'Предыдущие 5', next_5: 'Следующие 5'}}
          />
        </div>
        <div className="col-md-12">
          <Table
            _config={this.state.config}
            _data={this.state.data}
            actionFunc={(el)=> this.handlerActions(el)}
            actions={[
              {
                id:0,
                icon:"glyphicon-pencil",
                method:"GET",
                name:"Редактировать",
                url:"news"
              },
              {
                id:99,
                icon:"glyphicon-trash",
                method:"POST",
                name:"Удалить",
                url:"newsdel"
              }
            ]}
          />
        </div>
      </div>
    ]
  }
}
