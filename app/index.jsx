import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { HashRouter, Route, Switch, Link } from 'react-router-dom';

import AdminLayout from './layouts/AdminLayout.jsx';

import Home from './containers/home/index.jsx';
import Pages from './containers/pages/index.jsx';
import Menus from './containers/menus/index.jsx';
import Menu_items from './containers/menus/menu_items.jsx';
import News from './containers/news/index.jsx';
import GetOne from './containers/getOne/index.jsx';
import User from './containers/user/index.jsx';
import Appeals from './containers/appeals/index.jsx';

import './style.scss';
import 'react-toastify/dist/ReactToastify.css';
import 'react-select2-wrapper/css/select2.css';
import 'react-widgets/dist/css/react-widgets.css';

import configureStore from './redux/configStore';
const store = configureStore();

const AppRoute = ({ component: Component, layout: Layout, ...rest }) => (
  <Route {...rest} render={props =>
      <Layout servicename={props.match.params.servicename ? props.match.params.servicename : null}>
          <Component {...props} />
      </Layout>
  } />
);

/*
  <AppRoute
    exact
    path='/dashboard/:servicename/:compname'
    layout={DashLayout}
    component={Template}
  />
*/

export default class RouterBox extends Component {
  render() {
    return (
      <Provider store={store}>
        <HashRouter>
          <Switch>
            <AppRoute exact path="/" layout={AdminLayout} component={Home} />
            <AppRoute exact path="/new" layout={AdminLayout} component={News} />
            <AppRoute exact path="/pages" layout={AdminLayout} component={Pages} />
            <AppRoute exact path="/menu" layout={AdminLayout} component={Menus} />
            <AppRoute exact path="/news" layout={AdminLayout} component={News} />
            <AppRoute exact path="/user" layout={AdminLayout} component={User} />
            <AppRoute exact path='/menu_items/:id' layout={AdminLayout} component={Menu_items} />
            <AppRoute exact path="/appeals" layout={AdminLayout} component={Appeals} />
            <AppRoute exact path='/:url' layout={AdminLayout} component={GetOne} />
            <AppRoute exact path='/:url/:id' layout={AdminLayout} component={GetOne} />
            <AppRoute exact path='/:url/:parent/:id' layout={AdminLayout} component={GetOne} />
            <Route exact component={NotFound} />
          </Switch>
        </HashRouter>
      </Provider>
    );
  }
}

class NotFound extends Component {
  render() {
    return (
      <div>
        404
      </div>
    )
  }
}

ReactDOM.render(
  <RouterBox />,
  document.getElementById('root')
);