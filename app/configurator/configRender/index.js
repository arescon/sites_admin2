import React from 'react';
import * as R from 'ramda';
import classnames from 'classnames';
import { toast } from 'react-toastify';
import { DropdownList, Multiselect } from 'react-widgets';

import enhance from './enhance';

const ConfigRender = ({
  config, parent, one, helper,
  handlerChange, handlerClick,
  handlerSelect, handlerFileSelect,
  getDataSelect
}) => {
  if(Array.isArray(config) && config.length > 0) {
    return R.filter(n => n.parent === parent, config).map((el, i)=>{
      let p = el.params;
      let m = el.mods || [];
      let visible = true;
      if(m.visible) {
        if(one[m.visible.data] === m.visible.value ) visible = true; else visible = false;
      }
      if(el.parent === el.name) {
        toast.error('LOOP! '+el.name);
        return false;
      }
      if(visible) {
        switch (el.block) {
          case 'div':
            return <div key={`rend_${i}`} className={classnames(p.css)}>
              <ConfigRender
                one={one || {}}
                helper={helper}
                config={config}
                parent={el.name}
                handlerChange={handlerChange}
                handlerClick={handlerClick}
                handlerSelect={handlerSelect}
                handlerFileSelect={handlerFileSelect}
                getDataSelect={getDataSelect}
              />
            </div>
            break;
          case 'label':
          case 'span':
            return <el.block key={`rend_${i}`} className={classnames(p.css)}>
              { p.val || 'Nan' }
            </el.block>
            break;
          case 'input':
            return <el.block key={`rend_${i}`}
                className={classnames(p.css)}
                defaultValue={p.def || ''}
                type={p.type || 'text'}
                data-el={p.val || false}
                data-parent={p.parent || null}
                placeholder={p.placeholder || '...'}
                value={
                  p.parent ?
                      one[p.parent] ?
                          one[p.parent][p.val] || ''
                        : ''
                    : one[p.val] || ''
                }
                disabled={p.disabled}
                onChange={handlerChange}
              />
              break;
          case 'textarea':
            return <el.block key={`rend_${i}`}
                className={classnames(p.css)}
                type={p.type || 'text'}
                data-el={p.val || false}
                data-parent={p.par || null}
                rows={p.rows || 1}
                placeholder={p.placeholder || '...'}
                value={one[p.val] || p.def}
                onChange={handlerChange}
                disabled={p.disabled}
              />
              break;
          case 'button':
            return <input
                key={`rend_${i}`}
                type='button'
                className={classnames(p.css)}
                name={el.name}
                onClick={(e)=>{
                  e.preventDefault();
                  handlerClick(el.name, el.onClick);
                }}
                value={p.text || 'Nan'}
                disabled={p.disabled}
              />
            break;
          case 'select2':
            if(!helper[el.name]) getDataSelect(el);
            if(p.multiply) {
              return <Multiselect
                  key={`rend_${i}`}
                  data={helper[el.name]}
                  valueField={p.api_id}
                  textField={p.api_label}
                  defaultValue={one[p.val]}
                  onChange={e=>handlerSelect(e, p.val, p.par, true)}
                />
            } else {
              return <DropdownList
                key={`rend_${i}`}
                data={helper[el.name]}
                valueField={p.api_id}
                textField={p.api_label}
                defaultValue={one[p.val]}
                onChange={e=>handlerSelect(e[p.api_id] || e, p.val, p.par)}
              />
            }
            break;
          case 'filepicker':
            return <div
                key={`rend_${i}`}
              >
                <input
                  type='file'
                  data-parent={p.parent || null}
                  onChange={e => handlerFileSelect(e, el)}
                />
              </div>
            break;
          default:
            break;
        }
      } else null;
    })
  } else return <p>....</p>
}

export default enhance(ConfigRender);