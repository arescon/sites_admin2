import { connect } from 'react-redux';
import { toast } from 'react-toastify';

import compose from 'recompose/compose';
import lifecycle from 'recompose/lifecycle';
import withState from 'recompose/withState';
import withHandlers from 'recompose/withHandlers';

import { Axios } from 'app/utils';
const api = new Axios();

import { changeConfig } from 'app/configurator/redux/actions';

export default compose(
  connect(
    state => ({
      confgs: state.confgs
    }),
    dispatch => ({
      changeConfig: (name, obj) => dispatch(changeConfig(name, obj))
    })
  ),

  withState('helper', 'handlerHelper', {}),

  withHandlers({
    handlerFileSelect: ({ changeOne, one}) => (e, el) => {
      let val = e.target.files;
      let p = el.params;

      let f = new FormData();

      f.append('attached', val[0]);

      api.post(`${p.api_url}`, f, {
        s: (res) => { 
          let val = res.data.id;
          let par = e.target.dataset.parent;
          let _new = {...one};

          if(par) {
            _new[par] = {
              [`${el}`]: val
            }
          } else _new[el] = val;
          changeOne(_new);
          e.target.value = null;
        },
        e: (er) => {
          console.log(er);
        }
      })
    },
    handlerChange: ({ changeOne, one }) => e => {
      e.preventDefault;
      let el = e.target.dataset.el;
      let par = e.target.dataset.parent;
      let val = e.target.value;
      let _new = {...one};

      if(par) {
        _new[par] = {
          [`${el}`]: val
        }
      } else _new[el] = val;
      changeOne(_new)
    },
    handlerSelect: ({ changeOne, one }) => (val, el, par, multy) => {
      let _val;
      if(multy) {
        _val = [];
        val.map(el => {
          _val.push(el.id);
        });
      } else _val = val;

      console.log(val);

      let _new = {...one};
      if(par) {
        _new[par] = {
          [`${el}`]: _val
        }
      } else _new[el] = _val;
      changeOne(_new)
    },
    getDataSelect: ({ helper, handlerHelper }) => (el) => {
      let p = el.params;
      if(!helper[el.name]) {
        if(p.api_url === 'year') {
          helper[el.name] = year_arr;
          handlerHelper(helper);
        } else {
          api.get(`${p.api_url}`, {}, {
            s: (res) => {
              let new_arr = [];
              res.data.outjson.map((el_s)=>{
                new_arr.push({
                  id: el_s[p.api_id],
                  text: el_s[p.api_label]
                })
              })
              helper[el.name] = res.data.outjson;
              handlerHelper(helper);
            },
            e: (er) => {
              
            }
          });
        }
      }
    },
    handlerClick: (props) => (name, params) => {
      const { one } = props;
      const { type, func_name, obj } = params;
      switch (type) {
        case 'action':
            props[func_name](name, obj ? one[obj] : one);
          break;
        default:
      }
    }
  }),

  lifecycle({
    componentDidMount() {
      const { config, helper } = this.props;
      // здесь нужны координаельные проверки чтобы все было по фен шую, а то уходит в бесконечный цикл
    }
  })
);

var currentTime = new Date()
var cur_year = currentTime.getFullYear()
var year_arr = [];

for (let i = 2010; i <= cur_year; i++) {
  year_arr.push(i)
}