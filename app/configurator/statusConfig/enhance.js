import { connect } from 'react-redux';
import { toast } from 'react-toastify';

import * as R from 'ramda';

import compose from 'recompose/compose';
import lifecycle from 'recompose/lifecycle';
import withState from 'recompose/withState';
import withHandlers from 'recompose/withHandlers';

export default compose(
  connect(
    state => ({
      config: state.confgs.config,
      name: state.confgs.name
    })
  ),

  withState('ready', 'changeReady', false),

  lifecycle({
    componentDidMount() {
      const { name, } = this.props;
      if(name)
        if(!R.isEmpty(name)) changeReady(true)
    },
    componentDidUpdate() {
      const { name } = this.props;
      console.log('upd status', name)
    }
  })
)