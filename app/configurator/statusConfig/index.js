import React from 'react';

import enhance from './enhance';

const Status = ({
  name
}) =>
  <div style={{
    position: 'fixed',
    bottom: '5px',
    right: '50px',
    background: '#808080',
    zIndex: 10001
  }}>
    <span style={name ? {
      background: '#0ddc43',
      color: 'white'
    } : {
      background: 'red',
      color: 'white'
    } }>Configurator: { name || 'empty'}</span>
  </div>

export default enhance(Status);