import { connect } from 'react-redux';
import { toast } from 'react-toastify';
import * as R from 'ramda';
import moment from 'moment';

import compose from 'recompose/compose';
import lifecycle from 'recompose/lifecycle';
import withState from 'recompose/withState';
import withHandlers from 'recompose/withHandlers';

import { changeConfig, handlerConfig, changeOneConfgs } from './redux/actions';

import { parseJson } from './index';

import { blocks } from './templates';

const _config = ([
  {
    block: 'div',
    params: {
      css: ['col-md-12']
    },
    content: [
      {
        block: 'div',
        params: {
          css: ['col-md-6 form-group']
        },
        content: [
          {
            block: 'label',
            params: {
              css: [],
              val: 'Выберите блок'
            }
          },
          {
            block: 'input',
            params: {
              css: ['form-control'],
              type: 'text',
              placeholder: 'Введите тип тега',
              val: 'block',
              def: ''
            }
          },
        ]
      },
      {
        block: 'div',
        params: {
          css: ['col-md-12']
        },
        mods: {
          visible: { // if(данные.block === div)
            data: 'block',
            value: 'div'
          }
        },
        content: [
          {
            block: 'label',
            params: {
              css: [],
              val: 'Настройка параметров'
            }
          },
          {
            block: 'div',
            params: {
              css: ['row']
            },
            content: [
              {
                block: 'div',
                params: {
                  css: ['col-md-12 form-group']
                },
                content: [
                  {
                    block: 'label',
                    params: {
                      css: [],
                      val: 'Задать стили'
                    }
                  },
                  {
                    block: 'input',
                    params: {
                      css: ['form-control'],
                      type: 'text',
                      parent: 'params',
                      placeholder: 'CSS стили, через пробел',
                      val: 'css',
                      def: ''
                    }
                  },
                ]
              }
            ]
          }
        ]
      }
    ]
  }
]);

export default compose(
  connect(
    state => ({
      config: state.confgs.config,
      name: state.confgs.name,
      conf: state.confgs.conf
    }),
    dispatch => ({
      changeConfig: (name, obj) => dispatch(changeConfig(name, obj)),
      handlerConfig: (obj) => dispatch(handlerConfig(obj)),
      handlerOne: (obj) => dispatch(changeOneConfgs(obj))
    })
  ),
  withState('ready', 'changeReady', false),
  withState('config', 'editConfig', []),
  withState('modal', 'changeModal', null),
  withState('newBlock', 'changeNewBlock', {}),

  withHandlers({
    handlerSaveNew: ({ newBlock, config, handlerConfig }) => (e)=> {
      e.preventDefault;
      let _new = {...config};
      _new.push(newBlock);
      changeOneConfgs(_new);
    }
  }),

  lifecycle({
    componentDidMount() {
      const { config, editConfig, handlerConfig, changeReady } = this.props;

      editConfig(parseJson(_config));

      if(!Array.isArray(config)) {
        handlerConfig([]);
        changeReady(true);
      }
    },
    componentDidUpdate() {
      const { config, handlerConfig, changeReady } = this.props;
      if(!Array.isArray(config)) {
        handlerConfig([]);
        changeReady(true);
      }
    }
  })
)
