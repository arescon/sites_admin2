import React from 'react';
import * as R from 'ramda';
import nanoid from 'nanoid';
import Modal from 'react-responsive-modal';

import enhance from './enhance';
import ConfigRender from './configRender';

import styles from './style.scss';

class Render_form extends React.Component {
  constructor(props) {
    super(props);
  }
  _block(name_modal, id) {
    const { _props, parent } = this.props;
    const { conf, newBlock, changeNewBlock, handlerSaveNew, config, changeModal, modal } = _props;
    return <React.Fragment>
      {
        !id ? <div className={'col-md-12 '+styles.plus_button} onClick={()=>changeModal(name_modal)}>
          <span>+</span>
        </div> : null
      }
      <Modal
        center
        styles={{ modal: { width: '100%', maxWidth: '800px' } }}
        open={modal === name_modal ? true : false}
        onClose={()=>changeModal(null)}
      >
        <div className='col-md-12'>
          <h3>{ !id ? 'Новый блок' : 'Редактирование блока'}</h3>
        </div>
        <div className='col-md-12'>
          <ConfigRender
            config={config}
            parent={parent}
            one={ !id ? newBlock : {}}
            changeOne={ !id ? changeNewBlock :  null} />
        </div>
        {
          !id ? 
            <div className='col-md-12'>
              <button className='btn btn-success' onClick={handlerSaveNew}>Сохранить</button>
            </div>
            : null
        }
      </Modal >
    </React.Fragment>
  }
  render() {
    const { _props, parent } = this.props;
    const { config, changeModal, modal } = _props;
    let _obj = R.filter(n=>n.parent === parent, config);
    return <React.Fragment>
      {
        (_obj.length > 0) ?
          <ConfigRender
            editable={true}
            config={config}
            parent={parent}
            one={ !id ? newBlock : []}
            changeOne={ !id ? changeNewBlock :  null}
          />
        : null
      }
      {
        this._block('new_bl_b', null)
      }
    </React.Fragment>
  }
}

const Configurator = (props) =>
  <div className='row'>
    <div className='col-md-12'>
      <h3>Configurator</h3>
    </div>
    <div className='col-md-12'>
      {
        props.name 
          ? <Render_form _props={props} parent={null}/>
          : <span>нет конфига</span>
      }
    </div>
  </div>

export default enhance(Configurator);

export function parseJson(json) {
  let _obj = [];
  
  t(json, null, _obj);
  
  function t(json, parent, n_obj) {
    json.map((el,i) => {
      setTimeout

      let new_name = nanoid();
      el.name = new_name;
      el.parent = parent;
      let s = R.omit(['content'], el);
      n_obj.push(s);
      if(el.content) t(el.content, new_name, n_obj);
    });
  }
  return _obj;
}