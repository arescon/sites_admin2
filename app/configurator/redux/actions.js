export const EDIT_CONFIG = 'EDIT_CONFIG';
export const EDIT_CONF = 'EDIT_CONF';
export const HANDLER_CONFIG = 'HANDLER_CONFIG';

export function changeConfig(name, obj) {
  return (dispatch) => {
    dispatch({
      type: EDIT_CONFIG,
      payload: {
        name,
        obj
      }
    })
  }
}

export function changeOneConfgs(obj) {
  return (dispatch) => {
    dispatch({
      type: EDIT_CONF,
      payload: obj
    })
  }
}

export function handlerConfig(obj) {
  console.log('re conf')
  return (dispatch) => {
    dispatch({
      type: HANDLER_CONFIG,
      payload: obj
    })
  }
}