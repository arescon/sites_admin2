import * as R from 'ramda';

import {
  EDIT_CONFIG,
  HANDLER_CONFIG,
  EDIT_CONF
} from './actions';

let initialState = {
  config: [],
  name: null,
  conf: {}
};

const handlers = (state = initialState, action) => {
  switch (action.type) {
    case EDIT_CONFIG:
      let n = {...state};
      n.config = Array.isArray(action.payload.obj) ? action.payload.obj : [];
      n.name = action.payload.name;
      return n;
      break;
    case HANDLER_CONFIG:
      return {...state, config: action.payload}
    case EDIT_CONF:
      return {...state, conf: action.payload}
    default:
      return state;
  }
};

export default handlers;