export const blocks = {
  div: {
    block: 'div',
    name: null,
    parent: null,
    params: {
      css: []
    }
  },
  label: {
    block: 'label',
    name: null,
    parent: null,
    params: {
      css: [],
      val: ''
    }
  },
  input: {
    block: 'input',
    name: null,
    parent: null,
    params: {
      css: ['form-control'],
      type: 'text',
      placeholder: '',
      val: null,
      def: null
    }
  },
  textarea: {
    block: 'textarea',
    name: null,
    parent: null,
    params: {
      css: ['form-control'],
      type: 'text',
      placeholder: '',
      val: '',
      def: '',
      row: 3
    }
  },
  select: {
    block: 'select',
    name: null,
    parent: null,
    params: {
      css: ['form-control'],
      multiply: false,
      placeholder: '',
      val: null,
      def: null,
      api_url: '',
      api_id: '',
      api_label: '' 
    }
  },
  button: {
    block: 'button',
    name: null,
    parent: null,
    params: {
      css: ['btn', 'btn-info'],
      text: 'button'
    },
    onClick: {
      type: 'action',
      func_name: 'changeConfig',
      obj: 'config'
    }
  },
}