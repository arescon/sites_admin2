import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { ToastContainer } from 'react-toastify';

import * as formActions from '../redux/actions/forms.js';

import Preloader from '../box/preloader/index.jsx';
import Header from '../inc/header.jsx';

const Version_app = 'v.0.2.3.5';

import { Axios } from '../utils';
let api = new Axios();
const prefix = '/api';

class AdminLayout extends Component {
  constructor(props){
    super(props);
    this.state = ({
      preloader: true,
      menu: [
        {
          id: 0,
          title: 'Настройки',
          url: ''
        },
        {
          id: 1,
          title: 'Страницы',
          url: 'pages'
        },
        {
          id: 2,
          title: 'Новости',
          url: 'news'
        },
        {
          id: 3,
          title: 'Баннеры',
          url: 'banners'
        },
        {
          id: 4,
          title: 'Меню',
          url: 'menu'
        },
        {
          id: 5,
          title: 'Обращения граждан',
          url: 'appeals'
        }
      ]
    });
  }
  componentWillMount() {
    api.get(`${prefix}/orgsettings`, {}, {
      s: () => {
        let p = this.props;
        p.dispatch(formActions.Preloader(false));
      }
    });
  }
  renderMenu() {
    if(Array.isArray(this.state.menu)) {
      if(this.state.menu.length > 0) {
        return this.state.menu.map(el=>{
          return (
            <li key={`${el.id}as`}>
              <Link to={`/${el.url}`}>
                <p>{el.title}</p>
              </Link>
            </li>
          )
        })
      }
    }
  }
  toLogin(ev){
    ev.preventDefault();
    api.get(`/atf/out`, {}, {
      s: (res) => {
        console.log('es')
      }
    });
  }
  render() {
    return [
      <Preloader visible={this.props.forms.preloader} key='ley_1'/>,
      <div key='ley_2' className='wrapper'>
        <ToastContainer autoClose={8000} />
        <div className='left_b'>
          <div className="logo">
            <a href="https://Jelata.ru" className="simple-text logo-normal">
              Jelata
            </a>
            <p className="simple-text logo-normal logo-version">
              { Version_app }
            </p>
          </div>
          <div className="sidebar-wrapper">
            <ul className="nav">
              { this.renderMenu() }
              <li>
                <a href='' onClick={(ev)=>this.toLogin(ev)} >
                  <p>Выйти</p>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div className='main'>
          {this.props.children}
        </div>
      </div>
    ]
  }
}

AdminLayout.propTypes = {
  forms: PropTypes.any,
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  return {
    forms: state.forms
  };
};

export default connect(mapStateToProps, dispatch => ({ dispatch }))(AdminLayout);