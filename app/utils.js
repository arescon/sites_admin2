import axios from 'axios';
import { toast } from 'react-toastify';

export function dateFormater(date, watch) {
    let c = new Date(date);
    let d = [];
    var o = {};
    let dc = new Date();
    if(watch){
        o = {
            year: 'numeric',
            month: 'long',
            day: 'numeric',
            timezone: 'UTC',
            hour: 'numeric',
            minute: 'numeric'
        };
        d.y = c.getFullYear();
        d.m = c.getMonth();
        d.d = c.getDate();
        d.h = c.getHours();
        d.min = c.getMinutes();
        dc = new Date(d.y, d.m, d.d, d.h, d.min);
    } else {
        o = {
            year: 'numeric',
            month: 'long',
            day: 'numeric'
        };
        d.y = c.getFullYear();
        d.m = c.getMonth();
        d.d = c.getDate();
        let dc = new Date(d.y, d.m, d.d);
    }

    return dc.toLocaleString('ru', o);
}

export function LastIndex(arr) {
  let last_element = arr[arr.length - 1];
  return last_element;
}

// const BASE_URL = 'http://185.117.153.61:5617';
const BASE_URL = 'http://api.jhospital.ru';

export class Axios {
  constructor() {
    axios.defaults.withCredentials = true;
  }
  post(url, data, callbacks, text) {
    axios.post(BASE_URL + url, data, {
      onUploadProgress: (ProgressEvent)=>{
        if (callbacks && callbacks.progress) {
          callbacks.progress(ProgressEvent);
        }
      }
    }).then(res => {
      switch (res.status) {
        case 200:
          if (callbacks && callbacks.s) {
            callbacks.s(res);
          }
          console.log(text || 'успешно');
          break;
        default:
            // console.log(res);
            break;
      }
    })
    .catch(error => {
      let error_text = 'Ошибка: ';
      if(error.response.data.message) {
        error_text = error_text + error.response.data.message;
      }
      toast.error(error_text);
      if (callbacks && callbacks.e)
        callbacks.e(error);
      else {
        console.log(error);
      }
    });
  }
    get(url, data, callbacks) {
        axios.get(BASE_URL + url, {
                params: data
            })
            .then(res => {
                switch (res.status) {
                    case 200:
                    case 304:
                        if (callbacks && callbacks.s)
                            callbacks.s(res);
                        break;
                    default:
                        //// console.log(res);
                        // if (callbacks && callbacks.e)
                        //     callbacks.e(res);
                        // else{
                        //     showErr(res.error.message);
                        // }
                        break
                }
            })
            .catch(error => {
              if (callbacks && callbacks.e) callbacks.e(error);
                else {
                  if(error.response) {
                    switch (error.response.status) {
                      case 401:
                        let s = `${BASE_URL}/atf/auth`;
                        location.href = s;
                        break;
                      default:
                        
                        break;
                    }
                  }
                }
            });
    }
    delete(url, callbacks) {
        axios.delete(BASE_URL + url)
            .then(res => {
                console.log('success');
                switch (res.status) {
                    case 200:
                        if (callbacks && callbacks.s)
                            callbacks.s(res);
                        break;
                    default:
                        null;
                        break;
                }
            })
            .catch(error => {
                console.log('error', error);
                if (callbacks && callbacks.e) callbacks.e(error);
            });
    }
}

/**
 * преобразование файла в base64
 * @param {file} file 
 * @param {callback} cb 
 */
export function getBase64(file, cb) {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
        cb(reader.result);
    };
    reader.onerror = function (error) {
        // console.log('Error: ', error);
    };
}

/**
 * Рандомное получение ID
 * @param {r} длина строки, def: 5
 * @return строка с длинной r или def 
 */
export function makeid(r) {
    let _r = 5;
    if(r) _r = r;
    let text = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  
    for (let i = 0; i < _r; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}

/**
 * экранирование
 * @param {string} строки для экранирования
 * @return строка с длинной r или def 
 */
export function escapeHtml (string) {
    let entityMap = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#39;',
        '/': '&#x2F;',
        '`': '&#x60;',
        '=': '&#x3D;'
    };
    return String(string).replace(/[&<>"'`=\/]/g, function (s) {
        return entityMap[s];
    });
}