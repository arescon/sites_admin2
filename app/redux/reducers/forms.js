import {
  BREAD_CHANGE,
  PRELOADER
} from '../actions/forms';

let initialState = {
  bread: {
    redirect: false,
    curUrl: '',
    breadcrumbs: null,
    toUrl: '',
  },
  preloader: true
};

export default function breadChange(state = initialState, action) {
  switch (action.type) {
    case BREAD_CHANGE: {
      return { ...state, bread: action.payload.bread };
    }
    case PRELOADER: {
      return { ...state, preloader: action.preloader };
    }
    default:
      return state;
  }
}