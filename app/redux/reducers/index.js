import { combineReducers } from 'redux';
import forms from './forms';
import app from './app';

import confgs from 'app/configurator/redux/confgs';

export default combineReducers({
    app,
    forms,
    confgs
})