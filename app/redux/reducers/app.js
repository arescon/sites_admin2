import {
  CHANGE_ONE,
} from 'actions/app';

let initialState = {
  one: null,
};

const handlers = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_ONE:
      return {...state, one: action.payload};
      break;
    default:
      return state;
  }
}

export default handlers