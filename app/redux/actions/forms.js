export const CHANGE_NEW = 'CHANGE_NEW';
export const BREAD_CHANGE = 'BREAD_CHANGE';
export const PRELOADER = 'PRELOADER';

export function BreadChange(data) {
  return (dispatch) => {
    dispatch({
      type: BREAD_CHANGE,
      payload: data
    })
  };
}

export function Preloader(tof) {
  return (dispatch) => {
    dispatch({
      type: PRELOADER,
      preloader: tof
    })
  };
}