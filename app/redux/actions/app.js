export const CHANGE_ONE = 'CHANGE_ONE'

export function changeOne(item) {
  return (dispatch) => {
    dispatch({
      type: CHANGE_ONE,
      payload: item
    })
  }
}
