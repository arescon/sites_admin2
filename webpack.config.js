var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

const srcPath = path.resolve(__dirname, 'app');

module.exports = {
  mode: 'production', // production or development
  entry: './app/index.jsx',
  output: {
    path: path.resolve(__dirname, './dist/'),
    filename: 'bundle.admin.js'
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        loaders: ['babel-loader'],
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract(['css-loader', 'postcss-loader']),
        //use: ["postcss-loader"]
      },
      {
        test: /\.scss$/,
        loaders: ['style-loader', 'css-loader?modules&importLoaders=1&localIdentName=[local]', 'postcss-loader', 'sass-loader'],
        exclude: /node_modules/
      },
      {
        test: /\.(png|woff|woff2|eot|ttf|svg)$/,
        loader: 'url-loader?limit=100000'
      },
      { test: /\.gif$/, loader: "url-loader?mimetype=image/png" },
    ]
  },
  plugins: [
    new ExtractTextPlugin('bundle.admin.css')
  ],

  resolve: {
    modules: [path.resolve('app/components'), 'node_modules'],
    
    alias: {
      app: srcPath,
      actions: srcPath+'/redux/actions'
    },
  },
};